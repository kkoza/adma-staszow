# Adma-staszow
### Start

Motyw kompilowany jest za pomocą webpacka.

Instalacja niezbędnych zależności (w folderze themes/indusza)
```sh
$ npm install
```

Kompilacja motywu w wersji DEV

```sh
$ npm run watch
```

Kompilacja motywu w wersji PROD

```sh
$ npm run build
```

### Moduły

Dodatkowe moduły powinny być tworzone za pomocą Composer'a.
```sh
$ composer init
```

