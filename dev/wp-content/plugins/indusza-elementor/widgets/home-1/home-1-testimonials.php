<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class Home1Testimonials extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Bdevs Elementor widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'home-1-testimonials';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Bdevs Elementor widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Home 1 - Testimonials Widget', 'bdevs-elementor' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Bdevs Slider widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-favorite';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Bdevs Slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'home-1' ];
	}

	public function get_keywords() {
		return [ 'testimonials' ];
	}

	public function get_script_depends() {
		return [ 'bdevs-elementor'];
	}

	// BDT Position
	protected function element_pack_position() {
	    $position_options = [
	        ''              => esc_html__('Default', 'bdevs-elementor'),
	        'top-left'      => esc_html__('Top Left', 'bdevs-elementor') ,
	        'top-center'    => esc_html__('Top Center', 'bdevs-elementor') ,
	        'top-right'     => esc_html__('Top Right', 'bdevs-elementor') ,
	        'center'        => esc_html__('Center', 'bdevs-elementor') ,
	        'center-left'   => esc_html__('Center Left', 'bdevs-elementor') ,
	        'center-right'  => esc_html__('Center Right', 'bdevs-elementor') ,
	        'bottom-left'   => esc_html__('Bottom Left', 'bdevs-elementor') ,
	        'bottom-center' => esc_html__('Bottom Center', 'bdevs-elementor') ,
	        'bottom-right'  => esc_html__('Bottom Right', 'bdevs-elementor') ,
	    ];

	    return $position_options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_testimonials',
			[
				'label' => esc_html__( 'Testimonials Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'top_title',
			[
				'label'       => __( 'Top Title', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Top Title', 'bdevs-elementor' ),
				'default'     => __( 'It is Top Title', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'sub_heading',
			[
				'label'       => __( 'Sub Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your sub heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Sub Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => esc_html__( 'Testimonial Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [
					[
						'name'    => 'tab_image',
						'label'   => esc_html__( 'Image', 'bdevs-elementor' ),
						'type'    => Controls_Manager::MEDIA,
						'dynamic' => [ 'active' => true ],
					],		
					[
						'name'        => 'tab_author',
						'label'       => esc_html__( 'Tab Author', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_position',
						'label'       => esc_html__( 'Tab Position', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_content',
						'label'       => esc_html__( 'Tab Content', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_icon',
						'label'       => esc_html__( 'Tab Icon', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
				],
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_top_title',
			[
				'label'   => esc_html__( 'Show Top Title', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_sub_heading',
			[
				'label'   => esc_html__( 'Show Sub Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_image',
			[
				'label'   => esc_html__( 'Show Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>

		<div class="section-full  p-t120 p-b90 testimonial-1-outer bg-white">
			<div class="container">
				<div class="section-content"> 
					<div class="row">
						<div class="col-xl-6 col-lg-12 col-md-12">
							<!-- TITLE START-->
							<div class="section-head left wt-small-separator-outer">
								<?php if (( '' !== $settings['top_title'] ) && ( $settings['show_top_title'] )): ?>
								<div class="wt-small-separator site-text-primary">
									<div class="sep-leaf-left"></div>
									<div><?php echo wp_kses_post($settings['top_title']); ?></div>
								</div>
								<?php endif; ?>
								<?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
								<h2 class="wt-title"><?php echo wp_kses_post($settings['heading']); ?></h2>
								<?php endif; ?>
							</div>
							<!-- TITLE END-->    						
						</div>
					</div>
					<?php if (( '' !== $settings['sub_heading'] ) && ( $settings['show_sub_heading'] )): ?>
					<div class="testimonial-title-large">
						<span><?php echo wp_kses_post($settings['sub_heading']); ?></span>
					</div> 
					<?php endif; ?>
					<div class="testimonial-1-content owl-carousel">
						<?php foreach ( $settings['tabs'] as $item ) : ?>
							<div class="item">
								<div class="testimonial-1 bg-white">
									<div class="testimonial-content">
										<div class="testimonial-detail clearfix">
											<?php if ( '' !== $item['tab_image']['url'] ) : ?>
												<div class="testimonial-pic-block"> 
													<div class="testimonial-pic">
														<img src="<?php echo wp_kses_post($item['tab_image']['url']); ?>" alt="">
													</div>
												</div>
											<?php endif; ?>                                       
											<div class="testimonial-info">
												<?php if ( '' !== $item['tab_author'] ) : ?>
													<span class="testimonial-name"><?php echo wp_kses_post($item['tab_author']); ?></span>
												<?php endif; ?>
												<?php if ( '' !== $item['tab_position'] ) : ?>
													<span class="testimonial-position"><?php echo wp_kses_post($item['tab_position']); ?></span>
												<?php endif; ?>
											</div>
										</div>
										<?php if ( '' !== $item['tab_content'] ) : ?>
											<div class="testimonial-text">
												<p><?php echo wp_kses_post($item['tab_content']); ?></p>
												<i class="<?php echo wp_kses_post($item['tab_icon']); ?>"></i>
											</div> 
										<?php endif; ?>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>                
		</div>

	<?php
	}
}