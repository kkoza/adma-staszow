<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class PageProject extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Bdevs Elementor widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'page-project';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Bdevs Elementor widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Page - Project Widget', 'bdevs-elementor' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Bdevs Slider widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-favorite';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Bdevs Slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'page' ];
	}

	public function get_keywords() {
		return [ 'team' ];
	}

	public function get_script_depends() {
		return [ 'bdevs-elementor'];
	}

	// BDT Position
	protected function element_pack_position() {
		$position_options = [
			''              => esc_html__('Default', 'bdevs-elementor'),
			'top-left'      => esc_html__('Top Left', 'bdevs-elementor') ,
			'top-center'    => esc_html__('Top Center', 'bdevs-elementor') ,
			'top-right'     => esc_html__('Top Right', 'bdevs-elementor') ,
			'center'        => esc_html__('Center', 'bdevs-elementor') ,
			'center-left'   => esc_html__('Center Left', 'bdevs-elementor') ,
			'center-right'  => esc_html__('Center Right', 'bdevs-elementor') ,
			'bottom-left'   => esc_html__('Bottom Left', 'bdevs-elementor') ,
			'bottom-center' => esc_html__('Bottom Center', 'bdevs-elementor') ,
			'bottom-right'  => esc_html__('Bottom Right', 'bdevs-elementor') ,
		];

		return $position_options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_project',
			[
				'label' => esc_html__( 'Project Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'chose_style',
			[
				'label'     => esc_html__( 'Chose Style', 'bdevs-elementor' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'project_type_1' => esc_html__( 'Project Area 1', 'bdevs-elementor' ),
					'project_type_2' => esc_html__( 'Project Area 2', 'bdevs-elementor' ),
				],
				'default'   => 'project_type_1',
			]
		);

		$this->add_control(
			'top_title',
			[
				'label'       => __( 'Top Title', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Top Title', 'bdevs-elementor' ),
				'default'     => __( 'It is Top Title', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);	

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'post_number',
			[
				'label'       => __( 'Post Number', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Post Number', 'bdevs-elementor' ),
				'default'     => __( '3', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'post_order',
			[
				'label'     => esc_html__( 'Post Order', 'bdevs-elementor' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'asc'  => esc_html__( 'ASC', 'bdevs-elementor' ),
					'desc' => esc_html__( 'DESC', 'bdevs-elementor' ),
				],
				'default'   => 'asc',
			]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section_project_2',
			[
				'label' => esc_html__( 'Project Area 2', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'post_number_2',
			[
				'label'       => __( 'Post Number', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Post Number', 'bdevs-elementor' ),
				'default'     => __( '3', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'post_order_2',
			[
				'label'     => esc_html__( 'Post Order', 'bdevs-elementor' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'asc'  => esc_html__( 'ASC', 'bdevs-elementor' ),
					'desc' => esc_html__( 'DESC', 'bdevs-elementor' ),
				],
				'default'   => 'asc',
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_top_title',
			[
				'label'   => esc_html__( 'Show Top Title', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		$chose_style = $settings['chose_style'];
		?>
		<!-- OUR TEAM START -->
		<?php if( $chose_style == 'project_type_1' ): ?>
		<div class="section-full p-t120 p-b90 projects-outer-full">
			<div class="container">
				<!-- TITLE START-->
				<div class="section-head center wt-small-separator-outer">
					<?php if (( '' !== $settings['top_title'] ) && ( $settings['show_top_title'] )): ?>
						<div class="wt-small-separator site-text-secondry">
							<div  class="sep-leaf-left"></div>
							<div><?php echo wp_kses_post($settings['top_title']); ?></div>
						</div>
					<?php endif; ?>
					<?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
						<h2 class="wt-title"><?php echo wp_kses_post($settings['heading']); ?> </h2>
					<?php endif; ?>
				</div>
				<!-- TITLE END-->
				<div class="section-content">
					<div class="project-box-style1-outer no-gutters  row clearfix d-flex justify-content-center flex-wrap m-b30">
						<?php 
						$order = $settings['post_order'];
						$post_number = $settings['post_number'];
						$wp_query = new \WP_Query(array('posts_per_page' => $post_number,'post_type' => 'project',  'orderby' => 'ID', 'order' => $order));

						while ($wp_query -> have_posts()) : $wp_query -> the_post(); 
							$project_image = get_post_meta(get_the_ID(),'_cmb_project_image', true);
							$project_topic = get_post_meta(get_the_ID(),'_cmb_project_topic', true);
							?>
							<div class=" col-xl-4 col-lg-4 col-md-6 col-sm-6">
								<div class="project-box-style1">
									<div class="project-content">
										<div class="project-title">
											<?php echo esc_attr($project_topic); ?>
										</div>
										<h4 class="project-title-large"><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
									</div>
									<div class="project-media">
										<img src="<?php echo wp_get_attachment_url($project_image);?>" alt="">
									</div>
									<div class="project-view">
										<a class="elem pic-long project-view-btn" href="<?php echo wp_get_attachment_url($project_image);?>" title="Energy" 
											data-lcl-txt="Regulatory Compliance System" data-lcl-author="someone" data-lcl-thumb="<?php echo wp_get_attachment_url($project_image);?>">
											<i></i>    
										</a> 
									</div>                                    
								</div>    
							</div>
						<?php endwhile; ?> 
					</div>                            
				</div>
			</div>      
		</div>
		<?php elseif( $chose_style == 'project_type_2' ): ?>
			<div class="section-full p-t120 p-b90 bg-white">
				<div class="container">

					<!-- PAGINATION START -->
					<div class="filter-wrap">
						<ul class="masonry-filter">
							<li class="active"><a data-filter="*" href="#"><?php echo esc_html__( 'All', 'indusza' ); ?></a></li>
							<?php  $categories = get_terms('type'); 
							foreach( (array)$categories as $categorie){
								$cat_name = $categorie->name; 
								$cat_slug = $categorie->slug;
								?>
								<li><a data-filter=".<?php echo $cat_slug ?>" href="#"><?php echo $cat_name ?></a></li>
							<?php } ?>
						</ul>
					</div>
					<!-- PAGINATION END -->

					<!-- PROJECT CONTENT START -->
					<div class="masonry-wrap row clearfix d-flex justify-content-center flex-wrap">
						<?php 
							$order_2 = $settings['post_order_2'];
							$post_number_2 = $settings['post_number_2'];
							$wp_query = new \WP_Query(array('posts_per_page' => $post_number_2,'post_type' => 'project',  'orderby' => 'ID', 'order' => $order_2));

							while ($wp_query -> have_posts()) : $wp_query -> the_post(); 
							$project_image_2 = get_post_meta(get_the_ID(),'_cmb_project_image_2', true);
							$project_topic = get_post_meta(get_the_ID(),'_cmb_project_topic', true);
							$cates = get_the_terms(get_the_ID(),'type');
							$cate_name ='';
							$cate_slug ='';
							foreach((array)$cates as $cate){
								if(count($cates)>0){
									$cate_name .= $cate->name.' ';
									$cate_slug .= $cate->slug.' ';    
								} 
							} 
							$i++;
								?>
							<div class="masonry-item <?php echo $cate_slug ?> col-xl-4 col-lg-4 col-md-6 m-b30">
								<div class="project-outer2">
									<div class="project-style-2">
										<div class="project-media">
											<img src="<?php echo wp_get_attachment_url($project_image_2);?>" alt="">
										</div>
										<div class="wt-info">
											<a href="<?php the_permalink();?>"><i class="fa fa-link"></i></a>
											<a class="elem pic-long" href="<?php echo wp_get_attachment_url($project_image_2);?>" title="Energy" 
												data-lcl-txt="Mechanical engineering" data-lcl-author="someone" data-lcl-thumb="<?php echo wp_get_attachment_url($project_image_2);?>">
												<i class="fa fa-expand"></i>    
											</a>                                                                      
										</div>                              
									</div>

									<div class="project-content">
										<span class="project-category"><?php echo esc_attr($project_topic); ?></span>    
										<h4 class="wt-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
									</div>
								</div>                          
							</div>
						<?php endwhile; ?> 
					</div>
					<!-- PROJECT CONTENT END --> 
				</div>
			</div>   
		<?php endif; ?>

		<?php
	}
}