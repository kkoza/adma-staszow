<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class PageVision extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Bdevs Elementor widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'page-vision';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Bdevs Elementor widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Page - Vision Widget', 'bdevs-elementor' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Bdevs Slider widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-favorite';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Bdevs Slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'page' ];
	}

	public function get_keywords() {
		return [ 'vision' ];
	}

	public function get_script_depends() {
		return [ 'bdevs-elementor'];
	}

	// BDT Position
	protected function element_pack_position() {
		$position_options = [
			''              => esc_html__('Default', 'bdevs-elementor'),
			'top-left'      => esc_html__('Top Left', 'bdevs-elementor') ,
			'top-center'    => esc_html__('Top Center', 'bdevs-elementor') ,
			'top-right'     => esc_html__('Top Right', 'bdevs-elementor') ,
			'center'        => esc_html__('Center', 'bdevs-elementor') ,
			'center-left'   => esc_html__('Center Left', 'bdevs-elementor') ,
			'center-right'  => esc_html__('Center Right', 'bdevs-elementor') ,
			'bottom-left'   => esc_html__('Bottom Left', 'bdevs-elementor') ,
			'bottom-center' => esc_html__('Bottom Center', 'bdevs-elementor') ,
			'bottom-right'  => esc_html__('Bottom Right', 'bdevs-elementor') ,
		];

		return $position_options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_vision',
			[
				'label' => esc_html__( 'Vision Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'image',
			[
				'label'   => esc_html__( 'Vision Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::MEDIA,
				'dynamic' => [ 'active' => true ],
				'description' => esc_html__( 'Add Your Vision Image', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => esc_html__( 'Vision Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [	
					[
						'name'        => 'vision_select',
						'label'     => esc_html__( 'Vision Select', 'bdevs-elementor' ),
						'type'      => Controls_Manager::SELECT,
						'dynamic' => [ 'active' => true ],
						'options'   => [
							'1'  => esc_html__( 'Type 1', 'bdevs-elementor' ),
							'2'  => esc_html__( 'Type 2', 'bdevs-elementor' ),
						],
						'default'   => '1',
					],
					[
						'name'        => 'tab_title',
						'label'       => esc_html__( 'Tab Title', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_subtitle',
						'label'       => esc_html__( 'Tab Subtitle', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_link',
						'label'       => esc_html__( 'Tab Link', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_icon',
						'label'       => esc_html__( 'Tab Icon', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
				],
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_top_title',
			[
				'label'   => esc_html__( 'Show Sub Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_image',
			[
				'label'   => esc_html__( 'Show Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_year',
			[
				'label'   => esc_html__( 'Show Year', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);


		$this->add_control(
			'show_content',
			[
				'label'   => esc_html__( 'Show Content', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_button',
			[
				'label'   => esc_html__( 'Show Button', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>

		<?php if (( '' !== $settings['image']['url'] ) && ( $settings['show_image'] )): ?>
		<div class="section-full p-t120  p-b90 site-bg-white" style="background-image: url(<?php echo wp_kses_post($settings['image']['url']); ?>);">
		<?php endif; ?>
			<div class="container">
				<div class="row d-flex justify-content-center">
					<?php foreach ( $settings['tabs'] as $item ) : ?>
					<?php if( wp_kses_post($item['vision_select']) == '1'): ?>
					<div class="col-lg-4 col-md-6 m-b30">
						<div class="service-icon-box-two text-center bg-white">
							<?php if ( '' !== $item['tab_icon'] ) : ?>
							<div class="wt-icon-box-wraper">
								<div class="icon-xl inline-icon">
									<span class="icon-cell site-text-primary"><i class="<?php echo wp_kses_post($item['tab_icon']); ?>"></i></span>
								</div>
							</div> 
							<?php endif; ?>
							<?php if ( '' !== $item['tab_title'] ) : ?>
							<div class="service-icon-box-title">
								<h4 class="wt-title"><a href="<?php echo wp_kses_post($item['tab_link']); ?>"><?php echo wp_kses_post($item['tab_title']); ?></a></h4>
							</div>
							<?php endif; ?>
							<?php if ( '' !== $item['tab_subtitle'] ) : ?>
							<div class="service-icon-box-content">
								<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
							</div>
							<?php endif; ?>
						</div>
					</div>
					<?php elseif( wp_kses_post($item['vision_select']) == '2'): ?>
						<div class="col-lg-4 col-md-6 m-b30">
							<div class="service-icon-box-two text-center site-bg-black">
								<?php if ( '' !== $item['tab_icon'] ) : ?>
									<div class="wt-icon-box-wraper">
										<div class="icon-xl inline-icon">
											<span class="icon-cell site-text-primary"><i class="<?php echo wp_kses_post($item['tab_icon']); ?>"></i></span>
										</div>
									</div> 
								<?php endif; ?>
								<?php if ( '' !== $item['tab_title'] ) : ?>
									<div class="service-icon-box-title">
										<h4 class="wt-title"><a href="<?php echo wp_kses_post($item['tab_link']); ?>"><?php echo wp_kses_post($item['tab_title']); ?></a></h4>
									</div>
								<?php endif; ?>
								<?php if ( '' !== $item['tab_subtitle'] ) : ?>
									<div class="service-icon-box-content">
										<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
									</div>
								<?php endif; ?>
							</div>
						</div>
					<?php endif; ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>   

		
		<?php
	}
}