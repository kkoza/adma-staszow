<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class PageFAQ extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Bdevs Elementor widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'page-faq';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Bdevs Elementor widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Page - FAQ Widget', 'bdevs-elementor' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Bdevs Slider widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-favorite';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Bdevs Slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'page' ];
	}

	public function get_keywords() {
		return [ 'FAQ' ];
	}

	public function get_script_depends() {
		return [ 'bdevs-elementor'];
	}

	// BDT Position
	protected function element_pack_position() {
	    $position_options = [
	        ''              => esc_html__('Default', 'bdevs-elementor'),
	        'top-left'      => esc_html__('Top Left', 'bdevs-elementor') ,
	        'top-center'    => esc_html__('Top Center', 'bdevs-elementor') ,
	        'top-right'     => esc_html__('Top Right', 'bdevs-elementor') ,
	        'center'        => esc_html__('Center', 'bdevs-elementor') ,
	        'center-left'   => esc_html__('Center Left', 'bdevs-elementor') ,
	        'center-right'  => esc_html__('Center Right', 'bdevs-elementor') ,
	        'bottom-left'   => esc_html__('Bottom Left', 'bdevs-elementor') ,
	        'bottom-center' => esc_html__('Bottom Center', 'bdevs-elementor') ,
	        'bottom-right'  => esc_html__('Bottom Right', 'bdevs-elementor') ,
	    ];

	    return $position_options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_faq',
			[
				'label' => esc_html__( 'FAQ Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'top_title',
			[
				'label'       => __( 'Top Title', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Top Title', 'bdevs-elementor' ),
				'default'     => __( 'It is Top Title', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'tabs_1',
			[
				'label' => esc_html__( 'FAQ Column 1 Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [	
					[
						'name'        => 'faq_select',
						'label'     => esc_html__( 'FAQ Select', 'bdevs-elementor' ),
						'type'      => Controls_Manager::SELECT,
						'dynamic' => [ 'active' => true ],
						'options'   => [
							'1'  => esc_html__( 'Type 1', 'bdevs-elementor' ),
							'2'  => esc_html__( 'Type 2', 'bdevs-elementor' ),
						],
						'default'   => '2',
					],
					[
						'name'        => 'tab_id',
						'label'       => esc_html__( 'ID', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true,
					],
					[
						'name'        => 'tab_title',
						'label'       => esc_html__( 'Title', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( 'Tab Title' , 'bdevs-elementor' ),
						'label_block' => true,
					],
					[
						'name'        => 'tab_subtitle',
						'label'       => esc_html__( 'Subtitle', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( 'Tab Subtitle' , 'bdevs-elementor' ),
						'label_block' => true,
					],
				],
			]
		);

		$this->add_control(
			'tabs_2',
			[
				'label' => esc_html__( 'FAQ Column 2 Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [	
					[
						'name'        => 'faq_select',
						'label'     => esc_html__( 'FAQ Select', 'bdevs-elementor' ),
						'type'      => Controls_Manager::SELECT,
						'dynamic' => [ 'active' => true ],
						'options'   => [
							'1'  => esc_html__( 'Type 1', 'bdevs-elementor' ),
							'2'  => esc_html__( 'Type 2', 'bdevs-elementor' ),
						],
						'default'   => '2',
					],
					[
						'name'        => 'tab_id',
						'label'       => esc_html__( 'ID', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true,
					],
					[
						'name'        => 'tab_title',
						'label'       => esc_html__( 'Title', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( 'Tab Title' , 'bdevs-elementor' ),
						'label_block' => true,
					],
					[
						'name'        => 'tab_subtitle',
						'label'       => esc_html__( 'Subtitle', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( 'Tab Subtitle' , 'bdevs-elementor' ),
						'label_block' => true,
					],
				],
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_top_title',
			[
				'label'   => esc_html__( 'Show Top title', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>
		<!-- HELP SECTION START -->
		<div class="section-full p-t120 p-b90 bg-white">
			<div class="container">
				<!-- TITLE START-->
				<div class="section-head center wt-small-separator-outer text-center text-white">
					<?php if (( '' !== $settings['top_title'] ) && ( $settings['show_top_title'] )): ?>
					<div class="wt-small-separator site-text-secondry">
						<div  class="sep-leaf-left"></div>
						<div><?php echo wp_kses_post($settings['top_title']); ?></div>
					</div>
					<?php endif; ?>
					<?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
					<h2><?php echo wp_kses_post($settings['heading']); ?></h2>
					<?php endif; ?>
				</div>
				<!-- TITLE END-->                        

				<div class="section-content"> 
					<div class="faq-outer"> 
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<!-- Accordian -->
								<div class="wt-accordion  faq-accorfion m-b30" id="accordion5">

									<?php foreach ( $settings['tabs_1'] as $item ) : ?>
										<?php if( wp_kses_post($item['faq_select']) == '1'): ?>
											<div class="panel wt-panel">
												<div class="acod-head  acc-actives">
													<?php if ( '' !== $item['tab_title'] ) : ?>
														<h4 class="acod-title">
															<a data-toggle="collapse" href="#<?php echo wp_kses_post($item['tab_id']); ?>" data-parent="#accordion5" > 
																<?php echo wp_kses_post($item['tab_title']); ?>
																<span class="indicator"><i class="fa fa-plus"></i></span>
															</a>
														</h4>
													<?php endif; ?>
												</div>
												<div id="<?php echo wp_kses_post($item['tab_id']); ?>" class="acod-body collapse show">
													<?php if ( '' !== $item['tab_subtitle'] ) : ?>
														<div class="acod-content">
															<p>
																<?php echo wp_kses_post($item['tab_subtitle']); ?>
															</p>
														</div>
													<?php endif; ?>
												</div>
											</div>      
											<?php elseif( wp_kses_post($item['faq_select']) == '2'): ?>            
												<div class="panel wt-panel">
												<div class="acod-head  acc-actives">
													<?php if ( '' !== $item['tab_title'] ) : ?>
														<h4 class="acod-title">
															<a data-toggle="collapse" href="#<?php echo wp_kses_post($item['tab_id']); ?>" data-parent="#accordion5" > 
																<?php echo wp_kses_post($item['tab_title']); ?>
																<span class="indicator"><i class="fa fa-plus"></i></span>
															</a>
														</h4>
													<?php endif; ?>
												</div>
												<div id="<?php echo wp_kses_post($item['tab_id']); ?>" class="acod-body collapse">
													<?php if ( '' !== $item['tab_subtitle'] ) : ?>
														<div class="acod-content">
															<p>
																<?php echo wp_kses_post($item['tab_subtitle']); ?>
															</p>
														</div>
													<?php endif; ?>
												</div>
											</div>   
										<?php endif; ?>               
								<?php endforeach; ?>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<!-- Accordian -->
								<div class="wt-accordion  faq-accorfion m-b30" id="accordion6">

									<?php foreach ( $settings['tabs_2'] as $item ) : ?>
										<?php if( wp_kses_post($item['faq_select']) == '1'): ?>
											<div class="panel wt-panel">
												<div class="acod-head  acc-actives">
													<?php if ( '' !== $item['tab_title'] ) : ?>
														<h4 class="acod-title">
															<a data-toggle="collapse" href="#<?php echo wp_kses_post($item['tab_id']); ?>" data-parent="#accordion6" > 
																<?php echo wp_kses_post($item['tab_title']); ?>
																<span class="indicator"><i class="fa fa-plus"></i></span>
															</a>
														</h4>
													<?php endif; ?>
												</div>
												<div id="<?php echo wp_kses_post($item['tab_id']); ?>" class="acod-body collapse show">
													<?php if ( '' !== $item['tab_subtitle'] ) : ?>
														<div class="acod-content">
															<p>
																<?php echo wp_kses_post($item['tab_subtitle']); ?>
															</p>
														</div>
													<?php endif; ?>
												</div>
											</div>      
											<?php elseif( wp_kses_post($item['faq_select']) == '2'): ?>            
												<div class="panel wt-panel">
												<div class="acod-head  acc-actives">
													<?php if ( '' !== $item['tab_title'] ) : ?>
														<h4 class="acod-title">
															<a data-toggle="collapse" href="#<?php echo wp_kses_post($item['tab_id']); ?>" data-parent="#accordion6" > 
																<?php echo wp_kses_post($item['tab_title']); ?>
																<span class="indicator"><i class="fa fa-plus"></i></span>
															</a>
														</h4>
													<?php endif; ?>
												</div>
												<div id="<?php echo wp_kses_post($item['tab_id']); ?>" class="acod-body collapse">
													<?php if ( '' !== $item['tab_subtitle'] ) : ?>
														<div class="acod-content">
															<p>
																<?php echo wp_kses_post($item['tab_subtitle']); ?>
															</p>
														</div>
													<?php endif; ?>
												</div>
											</div>   
										<?php endif; ?>               
									<?php endforeach; ?>  
								</div>
							</div>
						</div>
					</div> 
				</div>                                 
			</div>
		</div>   

	<?php
	}
}