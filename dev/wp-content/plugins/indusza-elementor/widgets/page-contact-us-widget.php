<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class PageContact extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Bdevs Elementor widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'page-contact';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Bdevs Elementor widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Page - Contact Widget', 'bdevs-elementor' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Bdevs Slider widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-favorite';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Bdevs Slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'page' ];
	}

	public function get_keywords() {
		return [ 'Contact' ];
	}

	public function get_script_depends() {
		return [ 'bdevs-elementor'];
	}

	// BDT Position
	protected function element_pack_position() {
	    $position_options = [
	        ''              => esc_html__('Default', 'bdevs-elementor'),
	        'top-left'      => esc_html__('Top Left', 'bdevs-elementor') ,
	        'top-center'    => esc_html__('Top Center', 'bdevs-elementor') ,
	        'top-right'     => esc_html__('Top Right', 'bdevs-elementor') ,
	        'center'        => esc_html__('Center', 'bdevs-elementor') ,
	        'center-left'   => esc_html__('Center Left', 'bdevs-elementor') ,
	        'center-right'  => esc_html__('Center Right', 'bdevs-elementor') ,
	        'bottom-left'   => esc_html__('Bottom Left', 'bdevs-elementor') ,
	        'bottom-center' => esc_html__('Bottom Center', 'bdevs-elementor') ,
	        'bottom-right'  => esc_html__('Bottom Right', 'bdevs-elementor') ,
	    ];

	    return $position_options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_contact',
			[
				'label' => esc_html__( 'Contact Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'contact_form',
			[
				'label'       => __( 'Contact Form', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Contact Form', 'bdevs-elementor' ),
				'default'     => __( 'It is Contact Form', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'bg_image',
			[
				'label'   => esc_html__( 'Background Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::MEDIA,
				'dynamic' => [ 'active' => true ],
				'description' => esc_html__( 'Add Your Background Image', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => esc_html__( 'Address Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [			
					[
						'name'        => 'tab_title',
						'label'       => esc_html__( 'Title', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( 'Tab Title' , 'bdevs-elementor' ),
						'label_block' => true,
					],
					[
						'name'        => 'tab_subtitle',
						'label'       => esc_html__( 'Subtitle', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( 'Tab Subtitle' , 'bdevs-elementor' ),
						'label_block' => true,
					],
					
				],
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_iframe',
			[
				'label' => esc_html__( 'Map Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'iframe_link',
			[
				'label'       => __( 'Iframe Link', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Iframe Link', 'bdevs-elementor' ),
				'default'     => __( 'It is Iframe Link', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'iframe_width',
			[
				'label'       => __( 'Iframe Width', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your Iframe Width', 'bdevs-elementor' ),
				'default'     => __( 'It is Iframe Width', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'iframe_height',
			[
				'label'       => __( 'Iframe Height', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your Iframe Height', 'bdevs-elementor' ),
				'default'     => __( 'It is Iframe Height', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_image',
			[
				'label'   => esc_html__( 'Show Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_contact_form',
			[
				'label'   => esc_html__( 'Show Contact Form', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_iframe',
			[
				'label'   => esc_html__( 'Show Map', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>

		<!-- CONTACT FORM -->
		<div class="section-full  p-t120 p-b120">   
			<div class="section-content">
				<div class="container">
					<div class="contact-one">
						<!-- CONTACT FORM-->
						<div class="row no-gutters d-flex justify-content-center flex-wrap align-items-center">

							<div class="col-lg-7 col-md-12">
								<?php if (( '' !== $settings['contact_form'] ) && ( $settings['show_contact_form'] )): ?>
								<div class="contact-form-outer site-bg-gray">
									<?php echo do_shortcode(wp_kses_post($settings['contact_form'])); ?>
								</div>
								<?php endif; ?>
							</div> 

							<div class="col-lg-5 col-md-12">
								<?php if (( '' !== $settings['bg_image']['url'] ) && ( $settings['show_image'] )): ?>
								<div class="contact-info site-bg-black" style="background-image: url(<?php echo wp_kses_post($settings['bg_image']['url']); ?>);">
								<?php endif; ?>
								<?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
									<div class="section-head left wt-small-separator-outer when-bg-dark">
										<h3 class="wt-title"><?php echo wp_kses_post($settings['heading']); ?></h3>
									</div>
									<?php endif; ?>

									<div class="contact-info-section">  

										<?php foreach ( $settings['tabs'] as $item ) : ?>
										<div class="c-info-column">
											<?php if ( '' !== $item['tab_title'] ) : ?>
											<span class="m-t0"><?php echo wp_kses_post($item['tab_title']); ?></span>
											<?php endif; ?>
											<?php if ( '' !== $item['tab_subtitle'] ) : ?>
											<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
											<?php endif; ?>
										</div>  
										<?php endforeach; ?>

									</div>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>

		<!-- GOOGLE MAP -->
		<div class="section-full">
			<div class="container">   
				<div class="gmap-outline p-b120">
					<?php if (( '' !== $settings['iframe_link'] ) && ( $settings['show_iframe'] )): ?>
					<iframe src="<?php echo wp_kses_post($settings['iframe_link']); ?>" width="<?php echo wp_kses_post($settings['iframe_width']); ?>" height="<?php echo wp_kses_post($settings['iframe_height']); ?>" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
					<?php endif; ?>
				</div>
			</div>
		</div>      

	<?php
	}
}