<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class PageAbout extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Bdevs Elementor widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'page-about';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Bdevs Elementor widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Page - About Widget', 'bdevs-elementor' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Bdevs Slider widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-favorite';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Bdevs Slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'page' ];
	}

	public function get_keywords() {
		return [ 'about' ];
	}

	public function get_script_depends() {
		return [ 'bdevs-elementor'];
	}

	// BDT Position
	protected function element_pack_position() {
		$position_options = [
			''              => esc_html__('Default', 'bdevs-elementor'),
			'top-left'      => esc_html__('Top Left', 'bdevs-elementor') ,
			'top-center'    => esc_html__('Top Center', 'bdevs-elementor') ,
			'top-right'     => esc_html__('Top Right', 'bdevs-elementor') ,
			'center'        => esc_html__('Center', 'bdevs-elementor') ,
			'center-left'   => esc_html__('Center Left', 'bdevs-elementor') ,
			'center-right'  => esc_html__('Center Right', 'bdevs-elementor') ,
			'bottom-left'   => esc_html__('Bottom Left', 'bdevs-elementor') ,
			'bottom-center' => esc_html__('Bottom Center', 'bdevs-elementor') ,
			'bottom-right'  => esc_html__('Bottom Right', 'bdevs-elementor') ,
		];

		return $position_options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_about',
			[
				'label' => esc_html__( 'About Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'image',
			[
				'label'   => esc_html__( 'About Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::MEDIA,
				'dynamic' => [ 'active' => true ],
				'description' => esc_html__( 'Add Your About Image', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'top_title',
			[
				'label'       => __( 'Top Title', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your Top Title', 'bdevs-elementor' ),
				'default'     => __( 'It is Top Title', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'about_year',
			[
				'label'       => __( 'About Year', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your about year', 'bdevs-elementor' ),
				'default'     => __( 'It is About Year', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);	

		$this->add_control(
			'content',
			[
				'label'       => __( 'Content', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Content', 'bdevs-elementor' ),
				'default'     => __( 'It is Content', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);	

		$this->add_control(
			'link',
			[
				'label'       => __( 'Link', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Link', 'bdevs-elementor' ),
				'default'     => __( 'It is Link', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'button',
			[
				'label'       => __( 'Button', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Button', 'bdevs-elementor' ),
				'default'     => __( 'It is Button', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => esc_html__( 'About Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [	
					[
						'name'        => 'tab_title',
						'label'       => esc_html__( 'Tab Title', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_subtitle',
						'label'       => esc_html__( 'Tab Subtitle', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
				],
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_top_title',
			[
				'label'   => esc_html__( 'Show Sub Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_image',
			[
				'label'   => esc_html__( 'Show Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_year',
			[
				'label'   => esc_html__( 'Show Year', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);


		$this->add_control(
			'show_content',
			[
				'label'   => esc_html__( 'Show Content', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_button',
			[
				'label'   => esc_html__( 'Show Button', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>

		<div class="section-full p-t120 p-b90 bg-no-repeat bg-center bg-gray-light">
			<div class="about-section-one">
				<div class="container">
					<div class="section-content">                 
						<div class="row justify-content-center d-flex">

							<div class="col-lg-6 col-md-12 m-b30">
								<?php if (( '' !== $settings['image']['url'] ) && ( $settings['show_image'] )): ?>
								<div class="about-max-one">
									<div class="about-max-one-media"><img src="<?php echo wp_kses_post($settings['image']['url']); ?>" alt="" ></div>
								</div>
								<?php endif; ?>
							</div>  

							<div class="col-lg-6 col-md-12 m-b30">
								<div class="about-section-one-right">
									<!-- TITLE START-->
									<div class="section-head left wt-small-separator-outer">
										<?php if (( '' !== $settings['top_title'] ) && ( $settings['show_top_title'] )): ?>
											<div class="wt-small-separator site-text-primary">
												<div  class="sep-leaf-left"></div>
												<div><?php echo wp_kses_post($settings['top_title']); ?></div>
											</div>
										<?php endif; ?>
										<?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
											<h2><?php echo wp_kses_post($settings['heading']); ?></h2>
										<?php endif; ?>
									</div>
										
									<!-- TITLE END-->

									<div class="about-one">
										<div class="about-year">
											<?php if (( '' !== $settings['about_year'] ) && ( $settings['show_year'] )): ?>
											<div class="about-year-info">
												<?php echo wp_kses_post($settings['about_year']); ?>
											</div> 
											<?php endif; ?>
											<?php if (( '' !== $settings['content'] ) && ( $settings['show_content'] )): ?>
											<p><?php echo wp_kses_post($settings['content']); ?>
											</p>
											<?php endif; ?>
										</div>

										<div class="row"> 
											<?php foreach ( $settings['tabs'] as $item ) : ?>
											<div class="col-lg-6 col-md-12">
												<div class="wt-icon-box-wraper left bg-black m-b30">
													<div class="icon-content">
														<?php if ( '' !== $item['tab_title'] ) : ?>
														<h4 class="wt-tilte"><?php echo wp_kses_post($item['tab_title']); ?></h4>
														<?php endif; ?>
														<?php if ( '' !== $item['tab_subtitle'] ) : ?>
														<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
														<?php endif; ?>
													</div>
												</div>
											</div>
											<?php endforeach; ?>
										</div>
										<?php if (( '' !== $settings['button'] ) && ( $settings['show_button'] )): ?>
											<a href="<?php echo wp_kses_post($settings['link']); ?>" class="site-button sb-bdr-dark"><?php echo wp_kses_post($settings['button']); ?></a> 
										<?php endif; ?>	
									</div>
								</div>
							</div>
						</div>
					</div> 
				</div>
			</div>
		</div>   

		<?php
	}
}