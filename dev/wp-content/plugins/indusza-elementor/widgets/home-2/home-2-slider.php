<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class Home2Slider extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Bdevs Elementor widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'home-2-slider';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Bdevs Elementor widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Home 2 - Slider Widget', 'bdevs-elementor' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Bdevs Slider widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-favorite';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Bdevs Slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'home-2' ];
	}

	public function get_keywords() {
		return [ 'slider' ];
	}

	public function get_script_depends() {
		return [ 'bdevs-elementor'];
	}

	// BDT Position
	protected function element_pack_position() {
	    $position_options = [
	        ''              => esc_html__('Default', 'bdevs-elementor'),
	        'top-left'      => esc_html__('Top Left', 'bdevs-elementor') ,
	        'top-center'    => esc_html__('Top Center', 'bdevs-elementor') ,
	        'top-right'     => esc_html__('Top Right', 'bdevs-elementor') ,
	        'center'        => esc_html__('Center', 'bdevs-elementor') ,
	        'center-left'   => esc_html__('Center Left', 'bdevs-elementor') ,
	        'center-right'  => esc_html__('Center Right', 'bdevs-elementor') ,
	        'bottom-left'   => esc_html__('Bottom Left', 'bdevs-elementor') ,
	        'bottom-center' => esc_html__('Bottom Center', 'bdevs-elementor') ,
	        'bottom-right'  => esc_html__('Bottom Right', 'bdevs-elementor') ,
	    ];

	    return $position_options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_slider',
			[
				'label' => esc_html__( 'Slider Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => esc_html__( 'Slider Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [	
				    [
						'name'    => 'image',
						'label'   => esc_html__( 'Image', 'bdevs-elementor' ),
						'type'    => Controls_Manager::MEDIA,
						'dynamic' => [ 'active' => true ],
					],			
					[
						'name'        => 'id_1',
						'label'       => esc_html__( 'ID 1', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( 'ID 1' , 'bdevs-elementor' ),
						'label_block' => true,
					],
					[
						'name'        => 'id_2',
						'label'       => esc_html__( 'ID 2', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( 'ID 2' , 'bdevs-elementor' ),
						'label_block' => true,
					],
					[
						'name'        => 'heading',
						'label'       => esc_html__( 'Heading', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'       => 'sub_heading',
						'label'      => esc_html__( 'Sub Heading', 'bdevs-elementor' ),
						'type'       => Controls_Manager::TEXTAREA,
						'dynamic'    => [ 'active' => true ],
						'default'    => esc_html__( '', 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'       => 'link',
						'label'      => esc_html__( 'Link', 'bdevs-elementor' ),
						'type'       => Controls_Manager::TEXTAREA,
						'dynamic'    => [ 'active' => true ],
						'default'    => esc_html__( '', 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'       => 'button',
						'label'      => esc_html__( 'Button', 'bdevs-elementor' ),
						'type'       => Controls_Manager::TEXTAREA,
						'dynamic'    => [ 'active' => true ],
						'default'    => esc_html__( '', 'bdevs-elementor' ),
						'label_block' => true
					],
				],
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>

		<div class="slider-outer">
			<div id="welcome_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="goodnews-header" data-source="gallery" style="background:#eeeeee;padding:0px;">
				<div id="webmax-two" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.3.1">
					<ul>    
						<?php foreach ( $settings['tabs'] as $item ) : ?>
							<li data-index="<?php echo wp_kses_post($item['id_1']); ?>" 
							data-transition="fade" 
							data-slotamount="default" 
							data-hideafterloop="0" 
							data-hideslideonmobile="off"  
							data-easein="default" 
							data-easeout="default" 
							data-masterspeed="default"  
							data-thumb="<?php echo wp_kses_post($item['image']['url']); ?>"  
							data-rotate="0"  
							data-fstransition="fade" 
							data-fsmasterspeed="300" 
							data-fsslotamount="7" 
							data-saveperformance="off"  
							data-title="Slide Title" 
							data-param1="Additional Text" 
							data-param2="" 
							data-param3="" 
							data-param4="" 
							data-param5="" 
							data-param6="" 
							data-param7="" 
							data-param8="" 
							data-param9="" 
							data-param10="" 
							data-description="">
							<!-- MAIN IMAGE -->
							<?php if ( '' !== $item['tab_image']['url'] ) : ?>
							<img src="<?php echo wp_kses_post($item['image']['url']); ?>"  alt=""  data-lazyload="<?php echo wp_kses_post($item['image']['url']); ?>" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Power1.easeOut" data-scalestart="110" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
							<?php endif; ?>

							<!-- LAYER NR. 0 [ for overlay ] -->
							<div class="tp-caption tp-shape tp-shapewrapper " 
							id="<?php echo wp_kses_post($item['id_2']); ?>-layer-0" 
							data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
							data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
							data-width="full"
							data-height="full"
							data-whitespace="nowrap"
							data-type="shape" 
							data-basealign="slide" 
							data-responsive_offset="off" 
							data-responsive="off"
							data-frames='[
							{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},
							{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}
							]'
							data-textAlign="['left','left','left','left']"
							data-paddingtop="[0,0,0,0]"
							data-paddingright="[0,0,0,0]"
							data-paddingbottom="[0,0,0,0]"
							data-paddingleft="[0,0,0,0]"

							style="z-index: 1;background-color:rgba(0, 0, 0, 0);border-color:rgba(0, 0, 0, 0);border-width:0px;"> 
						</div>

						<!-- LAYERS 1 Black Circle -->
						<div class="tp-caption  tp-resizeme slider-block-2" 
						id="<?php echo wp_kses_post($item['id_2']); ?>-layer-1" 
						data-x="['left','left','center','center']" data-hoffset="['-120','-120','0','0']" 
						data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"                     
						data-type="button" 
						data-responsive_offset="on" 

						data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},
						{"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]'

						data-textAlign="['left','left','left','left']"
						data-paddingtop="[285,285,285,200]"
						data-paddingright="[285,285,285,200]"
						data-paddingbottom="[285,285,285,200]"
						data-paddingleft="[285,285,285,200]"

						style="z-index: 2;">
						<div class="rs-wave"  data-speed="1" data-angle="0" data-radius="2px"></div>
					</div>                                  

					<!-- LAYER NR. 2 [ for title ] -->
					<div class="tp-caption   tp-resizeme" 
					id="<?php echo wp_kses_post($item['id_2']); ?>-layer-2" 
					data-x="['left','left','center','center']" data-hoffset="[60','60','0','0']" 
					data-y="['middle','middle','top','top']" data-voffset="['-120','-120','160','240']"  
					data-fontsize="['20','20','20','14']"
					data-lineheight="['20','20','20','14']"
					data-width="['600','500','85%','96%']"
					data-height="['none','none','none','none']"
					data-whitespace="['normal','normal','normal','normal']"

					data-type="text" 
					data-responsive_offset="on" 
					data-frames='[
					{"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},
					{"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
					]'
					data-textAlign="['left','left','center','center']"
					data-paddingtop="[5,5,5,5]"
					data-paddingright="[0,0,0,0]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[0,0,0,0]"

					style="z-index: 2; 
					white-space: normal; 
					font-weight: 600;
					color:#fff;
					border-width:0px; font-family: 'Poppins', sans-serif; text-transform:uppercase">
					<?php if ( '' !== $item['sub_heading'] ) : ?>
					<div class="site-text-white"><?php echo wp_kses_post($item['sub_heading']); ?></div>
					<?php endif; ?>
				</div>

				<!-- LAYER NR. 3 [ for title ] -->
				<div class="tp-caption   tp-resizeme" 
				id="<?php echo wp_kses_post($item['id_2']); ?>-layer-3" 
				data-x="['left','left','center','center']" data-hoffset="[60','60','0','0']" 
				data-y="['middle','middle','middle','middle']" data-voffset="['0','0','-40','-40']"  
				data-fontsize="['80','70','60','30']"
				data-lineheight="['90','80','70','40']"
				data-width="['750','750','85%','96%']"
				data-height="['none','none','none','none']"
				data-whitespace="['normal','normal','normal','normal']"

				data-type="text" 
				data-responsive_offset="on" 
				data-frames='[
				{"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},
				{"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
				]'
				data-textAlign="['left','left','center','center']"
				data-paddingtop="[5,5,5,5]"
				data-paddingright="[0,0,0,0]"
				data-paddingbottom="[0,0,0,0]"
				data-paddingleft="[0,0,0,0]"

				style="z-index: 2; 
				white-space: normal; 
				font-weight: 700;
				color:#fff;
				border-width:0px; font-family: 'Poppins', sans-serif;">
				<?php if ( '' !== $item['heading'] ) : ?>
				<div class="site-text-white"><?php echo wp_kses_post($item['heading']); ?></div>
				<?php endif; ?>
			</div>                                

			<!-- LAYER NR. 5 [ for botton ] -->
			<div class="tp-caption tp-resizeme rev-btn"     
			id="<?php echo wp_kses_post($item['id_2']); ?>-layer-5"                      
			data-x="['left','left','center','center']" data-hoffset="['60','60','0','0']" 
			data-y="['middle','middle','middle','middle']" data-voffset="['150','150','120','60']"  
			data-lineheight="['none','none','none','none']"
			data-width="['300','300','300','300']"
			data-height="['none','none','none','none']"
			data-whitespace="['normal','normal','normal','normal']"

			data-type="text" 
			data-responsive_offset="on"
			data-frames='[ 
			{"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
			{"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
			]'
			data-textAlign="['left','left','center','center']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index:2;">
			<?php if ( '' !== $item['button'] ) : ?>
			<div><a href="<?php echo wp_kses_post($item['link']); ?>" class="site-button-secondry"><?php echo wp_kses_post($item['button']); ?></a></div>
			<?php endif; ?>
		</div>
	</li>

<?php endforeach; ?>

</ul>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> 
</div>
</div>

</div>

	<?php
	}
}