<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class Home2CounterVideo extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Bdevs Elementor widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'home-2-counter-video';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Bdevs Elementor widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Home 2 - Counter & Video Widget', 'bdevs-elementor' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Bdevs Slider widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-favorite';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Bdevs Slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'home-2' ];
	}

	public function get_keywords() {
		return [ 'Fact' ];
	}

	public function get_script_depends() {
		return [ 'bdevs-elementor'];
	}

	// BDT Position
	protected function element_pack_position() {
	    $position_options = [
	        ''              => esc_html__('Default', 'bdevs-elementor'),
	        'top-left'      => esc_html__('Top Left', 'bdevs-elementor') ,
	        'top-center'    => esc_html__('Top Center', 'bdevs-elementor') ,
	        'top-right'     => esc_html__('Top Right', 'bdevs-elementor') ,
	        'center'        => esc_html__('Center', 'bdevs-elementor') ,
	        'center-left'   => esc_html__('Center Left', 'bdevs-elementor') ,
	        'center-right'  => esc_html__('Center Right', 'bdevs-elementor') ,
	        'bottom-left'   => esc_html__('Bottom Left', 'bdevs-elementor') ,
	        'bottom-center' => esc_html__('Bottom Center', 'bdevs-elementor') ,
	        'bottom-right'  => esc_html__('Bottom Right', 'bdevs-elementor') ,
	    ];

	    return $position_options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_counter_video',
			[
				'label' => esc_html__( 'Counter & Video Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'image',
			[
				'label'   => esc_html__( 'Video Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::MEDIA,
				'dynamic' => [ 'active' => true ],
				'description' => esc_html__( 'Add Your Video Image', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'phone',
			[
				'label'       => __( 'Phone', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Phone', 'bdevs-elementor' ),
				'default'     => __( 'It is Phone', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'email',
			[
				'label'       => __( 'Email', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Email', 'bdevs-elementor' ),
				'default'     => __( 'It is Email', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'icon',
			[
				'label'       => __( 'Icon', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Icon', 'bdevs-elementor' ),
				'default'     => __( 'It is Icon', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'video_link',
			[
				'label'       => __( 'Video Link', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Video Link', 'bdevs-elementor' ),
				'default'     => __( 'It is Video Link', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);	

		$this->add_control(
			'tabs',
			[
				'label' => esc_html__( 'Counter Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [	
					[
						'name'        => 'tab_title',
						'label'       => esc_html__( 'Tab Title', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_number',
						'label'       => esc_html__( 'Tab Number', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_icon',
						'label'       => esc_html__( 'Tab Icon', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
				],
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_image',
			[
				'label'   => esc_html__( 'Show Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>

		<div class="section-full p-b90 site-bg-primary">
			<div class="container">
				<div class="section-content"> 
					<div class="c-section-two">
						<div class="counter-block-half">
							<div class="row justify-content-center d-flex no-gutters">
								<?php foreach ( $settings['tabs'] as $item ) : ?>
								<div class="col-lg-3 col-md-6 col-sm-6">
									<div class="wt-icon-box-wraper left">
										<?php if ( '' !== $item['tab_icon'] ) : ?>
										<span class="icon-md p-t10">
											<i class="<?php echo wp_kses_post($item['tab_icon']); ?>"></i>
										</span>
										<?php endif; ?>
										<div class="icon-content">
											<?php if ( '' !== $item['tab_number'] ) : ?>
											<div class="m-b5"><span class="counter"><?php echo wp_kses_post($item['tab_number']); ?></span></div>
											<?php endif; ?>
											<?php if ( '' !== $item['tab_title'] ) : ?>
											<div class="icon-content-info"><?php echo wp_kses_post($item['tab_title']); ?></div>
											<?php endif; ?>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
							</div>
						</div>

						<div class="video-with-content m-b30">
							<div class="row">
								<div class="col-lg-5 col-md-12">
									<div class="video-left-content">
										<?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
										<h3 class="wt-title site-text-white">
											<?php echo wp_kses_post($settings['heading']); ?>
										</h3>
										<?php endif; ?>
										<?php if ( '' !== $settings['phone'] ) : ?>
										<p><?php echo wp_kses_post($settings['phone']); ?></p> 
										<?php endif; ?>
										<?php if ( '' !== $settings['email'] ) : ?>
										<p><?php echo wp_kses_post($settings['email']); ?></p>
										<?php endif; ?>
										<?php if ( '' !== $settings['icon'] ) : ?>
										<i class="<?php echo wp_kses_post($settings['icon']); ?>"></i>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-lg-7 col-md-12">
									<?php if (( '' !== $settings['image']['url'] ) && ( $settings['show_image'] )): ?>
									<div class="video-section-two overlay-wraper bg-cover" style="background-image: url(<?php echo wp_kses_post($settings['image']['url']); ?>);">
									<?php endif; ?>
										<div class="overlay-main site-bg-black opacity-07"></div>
										<?php if ( '' !== $settings['video_link'] ) : ?>
										<a href="<?php echo wp_kses_post($settings['video_link']); ?>" class="mfp-video play-now-video">
											<i class="icon fa fa-play"></i>
											<span class="ripple"></span>
										</a>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>  

	<?php
	}
}