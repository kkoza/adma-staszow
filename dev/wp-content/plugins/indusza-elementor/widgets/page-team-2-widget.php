<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class PageTeam2 extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Bdevs Elementor widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'page-team-2';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Bdevs Elementor widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Page - Team 2 Widget', 'bdevs-elementor' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Bdevs Slider widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-favorite';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Bdevs Slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'page' ];
	}

	public function get_keywords() {
		return [ 'team 2' ];
	}

	public function get_script_depends() {
		return [ 'bdevs-elementor'];
	}

	// BDT Position
	protected function element_pack_position() {
	    $position_options = [
	        ''              => esc_html__('Default', 'bdevs-elementor'),
	        'top-left'      => esc_html__('Top Left', 'bdevs-elementor') ,
	        'top-center'    => esc_html__('Top Center', 'bdevs-elementor') ,
	        'top-right'     => esc_html__('Top Right', 'bdevs-elementor') ,
	        'center'        => esc_html__('Center', 'bdevs-elementor') ,
	        'center-left'   => esc_html__('Center Left', 'bdevs-elementor') ,
	        'center-right'  => esc_html__('Center Right', 'bdevs-elementor') ,
	        'bottom-left'   => esc_html__('Bottom Left', 'bdevs-elementor') ,
	        'bottom-center' => esc_html__('Bottom Center', 'bdevs-elementor') ,
	        'bottom-right'  => esc_html__('Bottom Right', 'bdevs-elementor') ,
	    ];

	    return $position_options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_team',
			[
				'label' => esc_html__( 'Team Area 2', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'top_title',
			[
				'label'       => __( 'Top Title', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Top Title', 'bdevs-elementor' ),
				'default'     => __( 'It is Top Title', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);	

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		 $this->add_control(
            'post_number',
            [
                'label'       => __( 'Post Number', 'bdevs-elementor' ),
                'type'        => Controls_Manager::TEXT,
                'placeholder' => __( 'Post Number', 'bdevs-elementor' ),
                'default'     => __( '3', 'bdevs-elementor' ),
                'label_block' => true,
            ]
        );

        $this->add_control(
            'post_order',
            [
                'label'     => esc_html__( 'Post Order', 'bdevs-elementor' ),
                'type'      => Controls_Manager::SELECT,
                'options'   => [
                    'asc'  => esc_html__( 'ASC', 'bdevs-elementor' ),
                    'desc' => esc_html__( 'DESC', 'bdevs-elementor' ),
                ],
                'default'   => 'asc',
            ]
        );

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_top_title',
			[
				'label'   => esc_html__( 'Show Top Title', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	
		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>
		<!-- OUR TEAM START -->
		<div class="section-full p-t120 p-b90 site-bg-gray-light">
			<div class="container">
				<div class="section-head center wt-small-separator-outer">
					<?php if (( '' !== $settings['top_title'] ) && ( $settings['show_top_title'] )): ?>
					<div class="wt-small-separator site-text-primary">
						<div class="sep-leaf-left"></div>
						<div><?php echo wp_kses_post($settings['top_title']); ?></div>
					</div>
					<?php endif; ?>
					<?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
					<h2 class="wt-title"><?php echo wp_kses_post($settings['heading']); ?></h2>
					<?php endif; ?>
				</div>           
				<div class="section-content">
					<div class="row justify-content-center">
							<?php 
							$order = $settings['post_order'];
							$post_number = $settings['post_number'];
							$wp_query = new \WP_Query(array('posts_per_page' => $post_number,'post_type' => 'team',  'orderby' => 'ID', 'order' => $order));

							while ($wp_query -> have_posts()) : $wp_query -> the_post(); 
								$team_image = get_post_meta(get_the_ID(),'_cmb_team_image', true);
								$team_position = get_post_meta(get_the_ID(),'_cmb_team_position', true);
								$team_facebook_link = get_post_meta(get_the_ID(),'_cmb_team_facebook_link', true);
								$team_twitter_link = get_post_meta(get_the_ID(),'_cmb_team_twitter_link', true);
								$team_linkedin_link = get_post_meta(get_the_ID(),'_cmb_team_linkedin_link', true);
								$team_pinterest_link = get_post_meta(get_the_ID(),'_cmb_team_pinterest_link', true);
								?>
							<div class="col-lg-4 col-md-6 col-sm-12 m-b30">
								<div class="wt-team-3">
									<div class="wt-media">
										<img src="<?php echo wp_get_attachment_url($team_image);?>" alt="">
									</div>   
									<div class="wt-info">
										<div class="team-detail">
											<span class="team-position"><?php echo esc_attr($team_position); ?></span>                                        
											<h4 class="m-t0 team-name"><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
											<div class="team-social-center">
												<ul class="team-social-bar">
													<li><a href="<?php echo esc_url($team_facebook_link); ?>"><i class="fa fa-facebook"></i></a></li>
													<li><a href="<?php echo esc_url($team_twitter_link); ?>"><i class="fa fa-twitter"></i></a></li>
													<li><a href="<?php echo esc_url($team_linkedin_link); ?>"><i class="fa fa-linkedin"></i></a></li>
													<li><a href="<?php echo esc_url($team_pinterest_link); ?>"><i class="fa fa-pinterest"></i></a></li>
												</ul>
											</div>                                             
										</div>
									</div>
								</div>
							</div>
						<?php endwhile; ?>       
					</div>
				</div>      
			</div>
		</div>   

	<?php
	}
}