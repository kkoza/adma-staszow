<?php
// register post type Portfolio

add_action( 'init', 'register_indusza_Project' );
function register_indusza_Project() {
    
    $labels = array( 
        'name' => __( 'Project', 'indusza' ),
        'singular_name' => __( 'Project', 'indusza' ),
        'add_new' => __( 'Add New Project', 'indusza' ),
        'add_new_item' => __( 'Add New Project', 'indusza' ),
        'edit_item' => __( 'Edit Project', 'indusza' ),
        'new_item' => __( 'New Project', 'indusza' ),
        'view_item' => __( 'View Project', 'indusza' ),
        'search_items' => __( 'Search Project', 'indusza' ),
        'not_found' => __( 'No Project found', 'indusza' ),
        'not_found_in_trash' => __( 'No Project found in Trash', 'indusza' ),
        'parent_item_colon' => __( 'Parent Project:', 'indusza' ),
        'menu_name' => __( 'Project', 'indusza' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'List Project',
        'supports' => array( 'title', 'editor', 'thumbnail', 'comments'),
        'taxonomies' => array( 'project', 'type' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon'     => 'dashicons-editor-paste-text',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_style' => 'post'
    );

    register_post_type( 'Project', $args );
}
add_action( 'init', 'create_ProjectType_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it Skillss for your posts

function create_ProjectType_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like Skills
//first do the translations part for GUI

  $labels = array(
    'name' => __( 'Type', 'indusza' ),
    'singular_name' => __( 'Type', 'indusza' ),
    'search_items' =>  __( 'Search Type','indusza' ),
    'all_items' => __( 'All Type','indusza' ),
    'parent_item' => __( 'Parent Type','indusza' ),
    'parent_item_colon' => __( 'Parent Type:','indusza' ),
    'edit_item' => __( 'Edit Type','indusza' ), 
    'update_item' => __( 'Update Type','indusza' ),
    'add_new_item' => __( 'Add New Type','indusza' ),
    'new_item_name' => __( 'New Type Name','indusza' ),
    'menu_name' => __( 'Type','indusza' ),
  );     

// Now register the taxonomy

  register_taxonomy('type',array('Project',), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'type' ),
  ));

}


add_action( 'init', 'register_indusza_Team' );
function register_indusza_Team() {
    
    $labels = array( 
        'name' => __( 'Team', 'indusza' ),
        'singular_name' => __( 'Team', 'indusza' ),
        'add_new' => __( 'Add New Team', 'indusza' ),
        'add_new_item' => __( 'Add New Team', 'indusza' ),
        'edit_item' => __( 'Edit Team', 'indusza' ),
        'new_item' => __( 'New Team', 'indusza' ),
        'view_item' => __( 'View Team', 'indusza' ),
        'search_items' => __( 'Search Team', 'indusza' ),
        'not_found' => __( 'No Team found', 'indusza' ),
        'not_found_in_trash' => __( 'No Team found in Trash', 'indusza' ),
        'parent_item_colon' => __( 'Parent Team:', 'indusza' ),
        'menu_name' => __( 'Team', 'indusza' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'List Team',
        'supports' => array( 'title', 'editor', 'thumbnail', 'comments'),
        'taxonomies' => array( 'Team', 'type1' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-groups', 
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_style' => 'post'
    );

    register_post_type( 'Team', $args );
}
add_action( 'init', 'create_TeamType_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it Skillss for your posts

function create_TeamType_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like Skills
//first do the translations part for GUI

  $labels = array(
    'name' => __( 'Type', 'indusza' ),
    'singular_name' => __( 'Type', 'indusza' ),
    'search_items' =>  __( 'Search Type','indusza' ),
    'all_items' => __( 'All Type','indusza' ),
    'parent_item' => __( 'Parent Type','indusza' ),
    'parent_item_colon' => __( 'Parent Type:','indusza' ),
    'edit_item' => __( 'Edit Type','indusza' ), 
    'update_item' => __( 'Update Type','indusza' ),
    'add_new_item' => __( 'Add New Type','indusza' ),
    'new_item_name' => __( 'New Type Name','indusza' ),
    'menu_name' => __( 'Type','indusza' ),
  );     

// Now register the taxonomy

  register_taxonomy('type1',array('Team',), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'type1' ),
  ));


}


add_action( 'init', 'register_indusza_Service' );
function register_indusza_Service() {
    
    $labels = array( 
        'name' => __( 'Service', 'indusza' ),
        'singular_name' => __( 'Service', 'indusza' ),
        'add_new' => __( 'Add New Service', 'indusza' ),
        'add_new_item' => __( 'Add New Service', 'indusza' ),
        'edit_item' => __( 'Edit Service', 'indusza' ),
        'new_item' => __( 'New Service', 'indusza' ),
        'view_item' => __( 'View Service', 'indusza' ),
        'search_items' => __( 'Search Service', 'indusza' ),
        'not_found' => __( 'No Service found', 'indusza' ),
        'not_found_in_trash' => __( 'No Service found in Trash', 'indusza' ),
        'parent_item_colon' => __( 'Parent Service:', 'indusza' ),
        'menu_name' => __( 'Service', 'indusza' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'List Service',
        'supports' => array( 'title', 'editor', 'thumbnail', 'comments'),
        'taxonomies' => array( 'Service', 'type2' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-hammer', 
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_style' => 'post'
    );

    register_post_type( 'Service', $args );
}
add_action( 'init', 'create_ServiceType_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it Skillss for your posts

function create_ServiceType_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like Skills
//first do the translations part for GUI

  $labels = array(
    'name' => __( 'Type', 'indusza' ),
    'singular_name' => __( 'Type', 'indusza' ),
    'search_items' =>  __( 'Search Type','indusza' ),
    'all_items' => __( 'All Type','indusza' ),
    'parent_item' => __( 'Parent Type','indusza' ),
    'parent_item_colon' => __( 'Parent Type:','indusza' ),
    'edit_item' => __( 'Edit Type','indusza' ), 
    'update_item' => __( 'Update Type','indusza' ),
    'add_new_item' => __( 'Add New Type','indusza' ),
    'new_item_name' => __( 'New Type Name','indusza' ),
    'menu_name' => __( 'Type','indusza' ),
  );     

// Now register the taxonomy

  register_taxonomy('type2',array('Service',), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'type2' ),
  ));


}