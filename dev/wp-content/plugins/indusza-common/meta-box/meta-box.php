<?php
/**
 * Plugin Name: Meta Box
 * Plugin URI:  https://metabox.io
 * Description: Create custom meta boxes and custom fields in WordPress.
 * Version:     5.3.3
 * Author:      MetaBox.io
 * Author URI:  https://metabox.io
 * License:     GPL2+
 * Text Domain: meta-box
 * Domain Path: /languages/
 *
 * @package Meta Box
 */

if ( defined( 'ABSPATH' ) && ! defined( 'RWMB_VER' ) ) {
    register_activation_hook( __FILE__, 'rwmb_check_php_version' );

    /**
     * Display notice for old PHP version.
     */
    function rwmb_check_php_version() {
        if ( version_compare( phpversion(), '5.3', '<' ) ) {
            die( esc_html__( 'Meta Box requires PHP version 5.3+. Please contact your host to upgrade.', 'meta-box' ) );
        }
    }




    require_once dirname( __FILE__ ) . '/inc/loader.php';
    $rwmb_loader = new RWMB_Loader();
    $rwmb_loader->init();


    add_filter( 'rwmb_meta_boxes', function ( $meta_boxes ) {

    $prefix = '_cmb_';


  // Open Code


    $meta_boxes[] = array(

        'id'         => 'post_setting',

        'title'      => 'Post Setting',

        'pages'      => array('post'), // Post type

        'context'    => 'normal',

        'priority'   => 'high',

        'show_names' => true, // Show field names on the left

        //'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox

        'fields' => array(

            array(

                'name' => 'Recent Image',

                'desc' => 'Show Recent Image',

                'id'   => $prefix . 'recent_image',

                'type'    => 'image',

            ),

            array(

                'name' => 'Recent Title',

                'desc' => 'Input Recent Title',

                'id'   => $prefix . 'recent_title',

                'type'    => 'textarea',

            ),

            array(

                'name' => 'Grid Image',

                'desc' => 'Show Grid Image',

                'id'   => $prefix . 'column_image',

                'type'    => 'image',

            ),

            array(

                'name' => 'Home Image',

                'desc' => 'Show Home Image',

                'id'   => $prefix . 'home_image',

                'type'    => 'image',

            ),

        )

    );


    $meta_boxes[] = array(

        'id'         => 'project_setting',

        'title'      => 'Project Setting',

        'pages'      => array('project'), // Post type

        'context'    => 'normal',

        'priority'   => 'high',

        'show_names' => true, // Show field names on the left

        //'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox

        'fields' => array(

            array(

                'name' => 'Project Topic',

                'desc' => 'Input Project Topic',

                'id'   => $prefix . 'project_topic',

                'type'    => 'text',

            ),

            array(

                'name' => 'Project Excerpt',

                'desc' => 'Input Project Excerpt',

                'id'   => $prefix . 'project_excerpt',

                'type'    => 'textarea',

            ),

            array(

                'name' => 'Project Image 1',

                'desc' => 'Show Project Image 1',

                'id'   => $prefix . 'project_image',

                'type'    => 'image',

            ),

            array(

                'name' => 'Project Image 2',

                'desc' => 'Show Project Image 2',

                'id'   => $prefix . 'project_image_2',

                'type'    => 'image',

            ),

            array(

                'name' => 'Project Image 3',

                'desc' => 'Show Project Image 3',

                'id'   => $prefix . 'project_image_3',

                'type'    => 'image',

            ),

        )

    );


    $meta_boxes[] = array(

        'id'         => 'services_setting',

        'title'      => 'Services Setting',

        'pages'      => array('service'), // Post type

        'context'    => 'normal',

        'priority'   => 'high',

        'show_names' => true, // Show field names on the left

        //'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox

        'fields' => array(

            array(

                'name' => 'Services Icon',

                'desc' => 'Input Services Icon',

                'id'   => $prefix . 'services_icon',

                'type'    => 'text',

            ),

        )

    );

    $meta_boxes[] = array(

        'id'         => 'team_setting',

        'title'      => 'Team Setting',

        'pages'      => array('team'), // Post type

        'context'    => 'normal',

        'priority'   => 'high',

        'show_names' => true, // Show field names on the left

        //'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox

        'fields' => array(

            array(

                'name' => 'Team Position',

                'desc' => 'Input Team Position',

                'id'   => $prefix . 'team_position',

                'type'    => 'text',

            ),

            array(

                'name' => 'Team Image',

                'desc' => 'Input Team Image',

                'id'   => $prefix . 'team_image',

                'type'    => 'image',

            ),

            array(

                'name' => 'Facebook Link',

                'desc' => 'Input Facebook Link',

                'id'   => $prefix . 'team_facebook_link',

                'type'    => 'text',

            ),

            array(

                'name' => 'Twitter Link',

                'desc' => 'Input Twitter Link',

                'id'   => $prefix . 'team_twitter_link',

                'type'    => 'text',

            ),

            array(

                'name' => 'Linkedin Link',

                'desc' => 'Input Linkedin Link',

                'id'   => $prefix . 'team_linkedin_link',

                'type'    => 'text',

            ),

            array(

                'name' => 'Pinterest Link',

                'desc' => 'Input Pinterest Link',

                'id'   => $prefix . 'team_pinterest_link',

                'type'    => 'text',

            ),

        )

    );



// End Code
    return $meta_boxes;
});
}
