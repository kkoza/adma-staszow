<?php

/**
* Plugin Name: Arkon Job Offers
* Description: Plugin z ofertami pracy.
* Version: 1.0
* Author: Arkonsoft
* Author URI: https://arkonsoft.pl/
* Text Domain: arkon-job-offers
* Domain Path: /languages
*/ 


define('ARKON_JOB_OFFERS_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('ARKON_JOB_OFFERS_PLUGIN_URL', plugin_dir_url(__FILE__));
define('ARKON_JOB_OFFERS_TAXONOMY', 'arkon_job_offers_tax'); 
define('ARKON_JOB_OFFERS_CPT', 'arkon_job_offers_cpt');
define('ARKON_JOB_OFFERS_TRANSLATION_DOMAIN', 'arkon-job-offers');



require_once(ARKON_JOB_OFFERS_PLUGIN_DIR."classes/arkon-job-offers.php");
require_once(ARKON_JOB_OFFERS_PLUGIN_DIR."classes/arkon-job-offers-cpt.php");
require_once(ARKON_JOB_OFFERS_PLUGIN_DIR."classes/arkon-job-offers-polylang.php");
require_once(ARKON_JOB_OFFERS_PLUGIN_DIR."classes/arkon-job-offers-shortcode.php");
require_once(ARKON_JOB_OFFERS_PLUGIN_DIR."classes/arkon-job-offers-front.php");


$arkon_job_offers = new Arkon_Job_Offers();
$arkon_job_offers_cpt = new ArkonJobOffersCpt();
$arkon_job_offers_polylang = new ArkonJobOffersPolylang();
$arkon_job_offers_shortcode = new ArkonJobOffersShortcode();

