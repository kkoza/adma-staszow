<?php
$args = [
    'numberposts' => -1,
    'post_type'   => ARKON_JOB_OFFERS_CPT
];
  $job_offers = get_posts( $args );

?>
    <div class="arkon-job-offers">
        <?php foreach ($job_offers as $job_offer):?>
            <article class="list">
                <div class="list__title">
                    <?php echo get_the_title($job_offer); ?>
                </div>
                <div class="list__button">
                    <button role="button" class="arkon-job-offers-button" data-id="<?php echo esc_attr($job_offer->ID); ?>"> 
                        <?php esc_html_e('Apply','arkon-job-offers');?>
                    </button>
                </div>
            </article>
        <?php endforeach; ?>
    </div>





<div class="arkon-job-offers-modal micromodal-slide" id="arkon-job-offers-modal" aria-hidden="true">
    <div class="arkon-job-offers-modal__overlay" tabindex="-1" data-micromodal-close>
      <div class="arkon-job-offers-modal__container" role="dialog" aria-modal="true" aria-labelledby="arkon-job-offers-title">
        <header class="arkon-job-offers-modal__header">
          <h2 class="arkon-job-offers-modal__title" id="arkon-job-offers-title">
           
          </h2>
          <button class="arkon-job-offers-modal__close button-color" aria-label="Close modal" data-micromodal-close></button>
          
        </header>
        <div class="arkon-job-offers-modal-text">
        <main class="arkon-job-offers-modal__content" id="arkon-job-offers-content">
        
        </main>

        <div class="arkon-job-offers-modal__forms">
        

        <?php echo apply_shortcodes( '[contact-form-7 id="2673" title="Job-Offers Contact"]' ); ?>
        </div>

        </div>
        <footer class="arkon-job-offers-modal__footer">
         
        </footer>
      </div>
    </div>
    
    
    
</div>


  