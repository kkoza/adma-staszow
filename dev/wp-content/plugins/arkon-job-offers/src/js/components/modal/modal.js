import MicroModal from 'micromodal';

export default class JobOfferModal {

    init = () => {
        document.addEventListener('DOMContentLoaded', this.onReady);
    }

    onReady = () => {
        const buttons = document.querySelectorAll('.arkon-job-offers-button');

        buttons.forEach(element => {
            element.addEventListener('click', this.onClick);
        });
    }

    onClick = async (event) => {
        const id = event.target.getAttribute('data-id');
        const content = await this.getPostContent(id);
        const title = await this.getPostTitle(id);

        if(!content){
            return;
        }
        
        this.updateModalContent(content);
        this.updateModalTitle(title);
        this.updateModalJobValue(title);
        this.formCheck();
        this.showModal('arkon-job-offers-modal');
       

    }

    getPost = async (id) => {
        const api_url = document.querySelector('link[rel="https://api.w.org/"]').href;

        const response = await fetch(api_url + 'wp/v2/arkon_job_offers_cpt/' + id);

        if(!response.ok){

            return null;
        }

        const data = await response.json();
       
        return data;
    }

    getPostContent = async (id) => {
        const post = await this.getPost(id);

        if(!post){

            return false;
        }
        
        return post.content.rendered;

    }
    getPostTitle = async (id) => {
        const post = await this.getPost(id);

        if(!post){

            return false;
        }
        
        return post.title.rendered;

    }

    updateModalContent = (content) => {

        const contentWrapper= document.getElementById('arkon-job-offers-content');

        contentWrapper.innerHTML = content;

    }

    updateModalTitle= (title) => {

        const titleWrapper= document.getElementById('arkon-job-offers-title');

        titleWrapper.innerHTML = title;

    }

    updateModalJobValue= (title) => {

        const jobValue= document.getElementById('job-offers-value');

        jobValue.value = title;

    }

    formCheck = () => {
        const form = document.querySelector('form.wpcf7-form');

        if (!form.classList.contains("sent")) {
           
            return false;
        } 

        this.resetForm(form);

    }


    resetForm = (form) => {
        form.classList.remove("sent");
        form.classList.add("init");
        form.setAttribute("data-status", "init");
    }


    showModal = (modalId) => {

        MicroModal.show(modalId);

    }
}
