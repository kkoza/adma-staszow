<?php

class ArkonJobOffersPolylang {
    public function __construct() {
        add_action( 'init', [$this, 'Register_translations']);
        add_action( 'init', 'arkonJobOffersLoadTextDomain' );
    }

    public function Contents_to_translate() {
        $array = [
            'See also',
            'Back to page',
            'Home',
            'Sorry, nothing to display',
        ];
        return $array;

    }
    public function Register_translations() {
        if (function_exists( 'pll_register_string' )) {
            $translate = $this::Contents_to_translate();
            foreach($translate as $text) pll_register_string($text, $text, ARKON_JOB_OFFERS_TRANSLATION_DOMAIN);
        }
    }
 
    public function arkonJobOffersLoadTextDomain() {
        load_plugin_textdomain( 'arkon-job-offers', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' ); 
    }
} 

?>

