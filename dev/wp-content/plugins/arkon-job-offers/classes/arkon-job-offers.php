<?php

class Arkon_Job_Offers {
    public function __construct() {
        add_action('admin_enqueue_scripts', [$this, 'add_admin_js']);
        add_action('admin_enqueue_scripts', [$this, 'add_admin_css']);
        add_action( 'wp_enqueue_scripts', [$this, 'add_front_js'] );
        add_action( 'wp_enqueue_scripts', [$this, 'add_front_css'] );
    }

    public function add_admin_js() {
        wp_enqueue_media();
        wp_enqueue_script('arkon-job-offers-admin', ARKON_JOB_OFFERS_PLUGIN_URL . "assets/js/arkon-job-offers-admin.js", array('jquery'));
    }

    public function add_admin_css() {
        wp_enqueue_style('arkon-job-offers-admin', ARKON_JOB_OFFERS_PLUGIN_URL . "assets/css/arkon-job-offers-admin.css");
    }
    
    public function add_front_js() {
        wp_enqueue_script('arkon-job-offers-front', ARKON_JOB_OFFERS_PLUGIN_URL . "assets/js/arkon-job-offers-front.js");
    }

    public function add_front_css() {
        wp_enqueue_style('arkon-job-offers-front', ARKON_JOB_OFFERS_PLUGIN_URL . "assets/css/arkon-job-offers-front.css");
    }
}

?>