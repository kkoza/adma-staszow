<?php

class ArkonJobOffersShortcode {
    public function __construct() {
        add_action('init', [$this, 'addShortcode']);
    }

    public function showCatgories() { 
        ob_start();
        include( ARKON_JOB_OFFERS_PLUGIN_DIR . 'templates/shortcode.php');
        return ob_get_clean();
    }  

    public function addShortcode() {
        add_shortcode('arkon_job_offers', [$this, 'showCatgories']);
    }
}