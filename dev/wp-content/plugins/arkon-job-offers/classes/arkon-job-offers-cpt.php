<?php

class ArkonJobOffersCpt {

    public function __construct() {
        add_action('init', [$this, 'registerCpt']);
    }

    public function registerCpt() {
 
        $labels = array(
            'name'                => esc_html__( 'Job Offers', ARKON_JOB_OFFERS_TRANSLATION_DOMAIN ), 
            'menu_name'           => esc_html__( 'Job Offers', ARKON_JOB_OFFERS_TRANSLATION_DOMAIN ),
            'singular_name'       => esc_html__( 'Job Offer', ARKON_JOB_OFFERS_TRANSLATION_DOMAIN ),
            'add_new'             => esc_html__( 'New Job Offer', ARKON_JOB_OFFERS_TRANSLATION_DOMAIN ),
            'add_new_item'        => esc_html__( 'Add a New Job Offer', ARKON_JOB_OFFERS_TRANSLATION_DOMAIN ),
            'edit_item'           => esc_html__( 'Edit Job Offer', ARKON_JOB_OFFERS_TRANSLATION_DOMAIN ),
            'new_item'            => esc_html__( 'New Job Offer', ARKON_JOB_OFFERS_TRANSLATION_DOMAIN ),
            'view_item'           => esc_html__( 'View Job Offer', ARKON_JOB_OFFERS_TRANSLATION_DOMAIN ),
            'search_items'        => esc_html__( 'Search For Job Offers', ARKON_JOB_OFFERS_TRANSLATION_DOMAIN ),
            'not_found'           => esc_html__( 'No Job Offers Found', ARKON_JOB_OFFERS_TRANSLATION_DOMAIN ),
        );

        $supports = array(
            'title', 
            'editor', 
            'page-attributes',
        );
                
        $args = array(
            'label'               => esc_html__( 'Job Offers', ARKON_JOB_OFFERS_TRANSLATION_DOMAIN ),
            'description'         => '',
            'labels'              => $labels,
            'supports'            => $supports,
            'public'              => true,
            'menu_position'       => 4,
            'menu_icon'           => 'dashicons-admin-multisite',
            'show_in_rest'        => true,
            'show_ui'             => true,
            'rewrite'             => [
                'slug' => 'job offers',
                'with_front' => true,
            ],
            'taxonomies' => [ARKON_JOB_OFFERS_TAXONOMY],
        );
        
        register_post_type( ARKON_JOB_OFFERS_CPT, $args); 
       
    } 
    
}
?>