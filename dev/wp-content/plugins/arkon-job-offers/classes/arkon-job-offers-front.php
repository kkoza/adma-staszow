<?php

class ArkonJobOffersFront {
    public function __construct() {
        add_filter('gettext', [$this, 'PolylangFilter'], 20, 3);
    }

    public function PolylangFilter($translations, $text, $domain) {
        if ($domain == ARKON_JOB_OFFERS_TRANSLATION_DOMAIN && function_exists( 'pll__' )) {
            $translations = pll__($text, $domain);
        }
        return $translations;
    }
}