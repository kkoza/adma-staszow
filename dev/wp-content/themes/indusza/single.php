<?php
/**
 * The Template for displaying all single posts
 */
$indusza_redux_demo = get_option('redux_demo');
get_header(); ?>

<?php 
while (have_posts()): the_post();
    ?>

    <?php if(isset($indusza_redux_demo['blog_image']['url']) && $indusza_redux_demo['blog_image']['url'] != ''){?>
        <div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo esc_url($indusza_redux_demo['blog_image']['url']);?>);">
        <?php }else{?> 
            <div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo get_template_directory_uri();?>/assets/images/banner/1.jpg);">
            <?php } ?>
        <div class="overlay-main site-bg-black opacity-06"></div>
        <div class="container">
            <div class="wt-bnr-inr-entry">
                <div class="banner-title-outer">
                    <div class="banner-title-name">
                        <h2 class="wt-title"><?php if(isset($indusza_redux_demo['blog_details_title']) && $indusza_redux_demo['blog_details_title'] != ''){?>
                        <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['blog_details_title']));?>
                    <?php }else{?>
                        <?php echo esc_html__( 'Blog Single', 'indusza' );
                    }
                    ?></h2>
                    </div>
                </div>
                <!-- BREADCRUMB ROW -->                            
                <div>
                    <ul class="wt-breadcrumb breadcrumb-style-2">
                        <li><a href="<?php echo esc_url(home_url('/')); ?>"><?php echo esc_html__( 'Home', 'indusza' )?></a></li>
                        <li><?php if(isset($indusza_redux_demo['blog_details_title']) && $indusza_redux_demo['blog_details_title'] != ''){?>
                        <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['blog_details_title']));?>
                    <?php }else{?>
                        <?php echo esc_html__( 'Blog Single', 'indusza' );
                    }
                    ?></li>
                    </ul>
                </div>

                <!-- BREADCRUMB ROW END -->                        
            </div>
        </div>
    </div>
    <!-- INNER PAGE BANNER END -->

    <!-- OUR BLOG START -->
    <div class="section-full  p-t120 p-b90 bg-white">
        <div class="container">
            <!-- BLOG SECTION START -->
            <div class="section-content">
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 m-b30">
                        <!-- BLOG START -->
                        <div class="blog-post-single-outer">
                            <div class="blog-post-style-2 blog-post-single bg-white p-b30">     
                                <?php if (has_post_thumbnail()) { ?> 
                                    <div class="wt-post-media">
                                        <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id());?>" alt="">
                                    </div>                                        
                                <?php } ?>
                                <div class="wt-post-info">
                                    <div class="wt-post-meta ">
                                        <ul>
                                            <li class="post-author"><i class="fa fa-user"></i> <?php echo esc_html__( 'By', 'indusza' )?>  <?php the_author_posts_link(); ?></li>
                                            <li class="post-comment"><i class="fa fa-comments"></i> <?php comments_number( esc_html__('0 Comments', 'indusza'), esc_html__('1 Comment', 'indusza'), esc_html__('% Comments', 'indusza')); ?></li>
                                        </ul>
                                    </div>                         
                                    <div class="wt-post-title ">
                                        <h3 class="post-title"><?php the_title();?></h3>
                                    </div>
                                    <div class="wt-post-discription">
                                        <?php the_content(); ?>
                                        <?php wp_link_pages(); ?>
                                    </div>                                            
                                </div>
                            </div>
                            <div class="clear" id="comment-list">
                                <div class="comments-area" id="comments">
                                    <?php comments_template();?>
                                </div>
                            </div>
                        </div>                             
                    </div> 

                    <!-- SIDE BAR START -->
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 rightSidebar  m-b30">
                        <aside  class="side-bar">
                            <?php get_sidebar();?>
                        </aside>
                    </div>
                    <!-- SIDE BAR END -->                  
                </div>
            </div>
        </div>
    </div>   
<?php endwhile; ?>
<?php
get_footer();
?>