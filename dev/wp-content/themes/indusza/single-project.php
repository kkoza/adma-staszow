<?php
/**
 * The Template for displaying all single posts
 */
$indusza_redux_demo = get_option('redux_demo');
get_header(); ?>

<?php 
while (have_posts()): the_post();
    ?>

    <?php if(isset($indusza_redux_demo['blog_image']['url']) && $indusza_redux_demo['blog_image']['url'] != ''){?>
        <div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo esc_url($indusza_redux_demo['blog_image']['url']);?>);">
        <?php }else{?> 
            <div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo get_template_directory_uri();?>/assets/images/banner/1.jpg);">
            <?php } ?>
            <div class="overlay-main site-bg-black opacity-06"></div>
            <div class="container">
                <div class="wt-bnr-inr-entry">
                    <div class="banner-title-outer">
                        <div class="banner-title-name">
                            <h2 class="wt-title"><?php if(isset($indusza_redux_demo['project_details_title']) && $indusza_redux_demo['project_details_title'] != ''){?>
                        <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['project_details_title']));?>
                    <?php }else{?>
                        <?php echo esc_html__( 'Project Detail', 'indusza' );
                    }
                    ?></h2>
                        </div>
                    </div>
                    <!-- BREADCRUMB ROW -->     
                    <div>
                        <ul class="wt-breadcrumb breadcrumb-style-2">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>"><?php echo esc_html__( 'Home', 'indusza' )?></a></li>
                            <li><?php if(isset($indusza_redux_demo['project_details_title']) && $indusza_redux_demo['project_details_title'] != ''){?>
                        <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['project_details_title']));?>
                    <?php }else{?>
                        <?php echo esc_html__( 'Project Detail', 'indusza' );
                    }
                    ?></li>
                        </ul>
                    </div>
                    <!-- BREADCRUMB ROW END -->                        
                </div>
            </div>
        </div>
        <!-- INNER PAGE BANNER END -->

        <!-- info SECTION START -->
        <div class="section-full p-t60 p-b60 bg-gray">
            <div class="container">                               
                <div class="section-content"> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="project-single">
                                <?php if (has_post_thumbnail()) { ?> 
                                <div class="project-single-media m-b30">
                                    <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id());?>" alt="">
                                </div>
                                <?php } ?>
                                <div class="wt-info  bg-white">
                                    <h3 class="wt-tilte"><?php the_title();?></h3>
                                        <?php the_content(); ?>
                                        <?php wp_link_pages(); ?>
                                </div>                
                            </div>
                        </div>
                        <div class="col-lg-4 rightSidebar">
                            <?php get_sidebar('project');?>
                        </div>
                    </div>
                </div>
            </div>    
        </div>   
        <!-- info  SECTION END -->
    <?php endwhile; ?>

    <!-- PROJECT SECTION START -->
    <div class="section-full  p-t90 p-b90 site-bg-gray-light">
        <div class="container-fluid">
            <!-- TITLE START-->
            <div class="section-head center wt-small-separator-outer">
                <div class="wt-small-separator site-text-primary">
                    <div class="sep-leaf-left"></div>
                    <div><?php echo esc_html__( 'REALIZACJE', 'indusza' )?></div>
                </div>
                <h2 class="wt-title"><?php echo esc_html__( 'Zrealizowane projekty', 'indusza' )?></h2>
            </div>
            <!-- TITLE END-->

            <div class="section-content">     
                <div class="owl-carousel project-slider1  project-box-style1-outer m-b30">
                    <!-- COLUMNS 1 --> 
                    <?php 
                    $args = array(   
                        'post_type' => 'project', 
                        'posts_per_page' => 8,
                    );  
                    $wp_query = new WP_Query($args);
                    while ($wp_query -> have_posts()) : $wp_query -> the_post();
                        $project_image = get_post_meta(get_the_ID(),'_cmb_project_image', true);
                        $project_topic = get_post_meta(get_the_ID(),'_cmb_project_topic', true);
                        ?>
                    <div class="item ">
                        <div class="project-box-style1">
                            <div class="project-content">
                                <div class="project-title">
                                    <?php echo esc_attr($project_topic); ?>
                                </div>
                                <h4 class="project-title-large"><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
                            </div>
                            <div class="project-media">
                                <img src="<?php echo wp_get_attachment_url($project_image);?>" alt="">
                            </div>
                            <div class="project-view">
                                <a class="elem pic-long project-view-btn" href="<?php echo wp_get_attachment_url($project_image);?>" title="Energy" 
                                data-lcl-txt="Regulatory Compliance System" data-lcl-author="someone" data-lcl-thumb="<?php echo wp_get_attachment_url($project_image);?>">
                                <i></i>    
                            </a> 
                        </div>                                    
                    </div>
                </div>
                <?php endwhile; ?> 
            </div>
        </div>
    </div>
</div>  

<?php
get_footer();
?>