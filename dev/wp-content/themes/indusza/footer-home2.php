<?php $indusza_redux_demo = get_option('redux_demo'); ?>

</div>
        <!-- FOOTER START -->
        <footer class="site-footer footer-dark" >
            <!-- FOOTER BLOCKES START -->  
            <?php if(isset($indusza_redux_demo['footer_background']['url']) && $indusza_redux_demo['footer_background']['url'] != ''){?>
            <div class="footer-top bg-bottom-center bg-no-repeat" style="background-image:url(<?php echo esc_url($indusza_redux_demo['footer_background']['url']);?>);">
            <?php }else{ ?>
            <div class="footer-top bg-bottom-center bg-no-repeat" style="background-image:url(<?php echo get_template_directory_uri();?>/assets/images/background/footer-map.png);">
            <?php } ?>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <?php if ( is_active_sidebar( 'footer-area-1' ) ) : ?>
                                <?php dynamic_sidebar( 'footer-area-1' ); ?>
                            <?php endif; ?>   
                        </div>                        
                    
                        <div class="col-lg-3 col-md-6">
                            <?php if ( is_active_sidebar( 'footer-area-2' ) ) : ?>
                                <?php dynamic_sidebar( 'footer-area-2' ); ?>
                            <?php endif; ?>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <?php if ( is_active_sidebar( 'footer-area-3' ) ) : ?>
                                <?php dynamic_sidebar( 'footer-area-3' ); ?>
                            <?php endif; ?>
                        </div>

                       <div class="col-lg-3 col-md-6">  
                            <?php if ( is_active_sidebar( 'footer-area-4' ) ) : ?>
                                <?php dynamic_sidebar( 'footer-area-4' ); ?>
                            <?php endif; ?>                                                       
                       </div> 
                    </div>
                    <div class="footer_blocks">
                        <div class="row justify-content-center no-gutters">

                            <!--Block 1-->
                            <div class="col-lg-4 col-md-4">
                                <div class="block-content">
                                    <?php if ( is_active_sidebar( 'footer-area-5' ) ) : ?>
                                        <?php dynamic_sidebar( 'footer-area-5' ); ?>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <!--Block 2-->
                            <div class="col-lg-4 col-md-4">
                                <div class="block-content">
                                    <?php if ( is_active_sidebar( 'footer-area-6' ) ) : ?>
                                        <?php dynamic_sidebar( 'footer-area-6' ); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            
                            <!--Block 3-->
                            <div class="col-lg-4 col-md-4">
                                <div class="block-content">
                                    <?php if ( is_active_sidebar( 'footer-area-7' ) ) : ?>
                                        <?php dynamic_sidebar( 'footer-area-7' ); ?>
                                    <?php endif; ?>
                                </div>
                            </div> 

                        </div>
                    </div>

                </div>
            </div>
            <!-- FOOTER COPYRIGHT -->
                                
            <div class="footer-bottom">
                <div class="container">
                    <div class="footer-bottom-info">
                        <div class="footer-copy-right">
                            <?php if(isset($indusza_redux_demo['footer_text'])){?>
                            <span class="copyrights-text"><?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['footer_text']));?></span>
                            <?php } ?>
                        </div>
                    </div>
                </div>   
            </div>   
    
        </footer>
        <!-- FOOTER END -->

        <!-- BUTTON TOP START -->
        <button class="scroltop"><span class="fa fa-angle-up  relative" id="btn-vibrate"></span></button>

    </div>

    <!-- LOADING AREA START ===== -->
        <div class="loading-area">
            <div class="loading-box"></div>
            <div class="loading-pic">
                <div class="cssload-spinner"></div>
            </div>
        </div>


<?php wp_footer(); ?>

</body>

</html>