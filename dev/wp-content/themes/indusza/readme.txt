/*
    Theme Name: Indusza
	Theme URI: http://shtheme.com/demosd/indusza
	Author: Shtheme
	Author URI: https://themeforest.net/user/shtheme
    Release Date: 25 January 2021
    Requirements: WordPress 5.6 or higher, PHP 5
    Compatibility: WordPress 5.6
    Tags: web app
    Last Update Date: 25 January 2021
*/

/**** Readme ****/

"Please backup your theme pack files at first before you update the theme into the latest version"


2021.01.25 - version 1.0.0
- First release.
