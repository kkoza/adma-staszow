<?php
/*
 * The Template for displaying all posts
 * 
 */
$indusza_redux_demo = get_option('redux_demo');
get_header(); ?>

<?php if(isset($indusza_redux_demo['blog_image']['url']) && $indusza_redux_demo['blog_image']['url'] != ''){?>
<div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo esc_url($indusza_redux_demo['blog_image']['url']);?>);">
<?php }else{?> 
<div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo (get_template_directory_uri().'/assets/images/banner/1.jpg');?>);">
<?php } ?>
    <div class="overlay-main site-bg-black opacity-06"></div>
    <div class="container">
        <div class="wt-bnr-inr-entry">
            <div class="banner-title-outer">
                <div class="banner-title-name">
                    <h2 class="wt-title"><?php printf( esc_html__( 'Tag Archives: %s', 'indusza' ), single_tag_title( '', false ) ); ?></h2>
                </div>
            </div>                     
        </div>
    </div>
</div>
<!-- INNER PAGE BANNER END -->

<!-- OUR BLOG START -->
<div class="section-full p-t120 p-b90 bg-white">
    <div class="container">
        <!-- BLOG SECTION START -->
        <div class="section-content">
            <div class="row d-flex justify-content-center">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 m-b30">
                    <div class="blog-post-2-outer blog-list-style">
                        <?php 
                        while (have_posts()): the_post();
                            ?>
                            <!--Block one-->
                            <div class="blog-post blog-post-4-outer m-b30">
                                <?php if (has_post_thumbnail()) { ?>
                                    <div class="wt-post-media wt-img-effect zoom-slow">
                                        <a href="<?php the_permalink();?>"><img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id());?>" alt=""></a>
                                    </div>
                                <?php } ?>                            
                                <div class="wt-post-info">
                                    <div class="wt-post-meta ">
                                        <ul>
                                            <li class="post-author"><i class="fa fa-user"></i> <?php echo esc_html__( 'By', 'indusza' )?>  <?php the_author_posts_link(); ?></li>
                                            <li class="post-comment"><i class="fa fa-comments"></i> <?php comments_number( esc_html__('0 Comments', 'indusza'), esc_html__('1 Comment', 'indusza'), esc_html__('% Comments', 'indusza')); ?></li>
                                        </ul>
                                    </div>                                 
                                    <div class="wt-post-title ">
                                        <h4 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
                                    </div>
                                    <div class="wt-post-text ">
                                        <p><?php if(isset($indusza_redux_demo['blog_excerpt'])){?>
                                            <?php echo esc_attr(indusza_excerpt($indusza_redux_demo['blog_excerpt'])); ?>
                                        <?php }else{?>
                                            <?php echo esc_attr(indusza_excerpt(40)); 
                                        }
                                        ?></p>
                                    </div>                                           
                                    <div class="wt-post-readmore ">
                                        <a href="<?php the_permalink();?>" class="site-button-link black"><?php echo esc_html__( 'Read More', 'indusza' );?></a>
                                    </div>                                        
                                </div>                                
                            </div>
                        <?php endwhile; ?> 
                    </div>
                    <!--Post pagination-->
                    <div class="pagination-outer text-center">
                        <div class="pagination-style1 text-center">
                            <?php indusza_pagination();?>
                        </div>
                    </div>
                </div> 

                <!-- SIDE BAR START -->
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 rightSidebar  m-b30">
                    <aside  class="side-bar">
                        <?php get_sidebar();?>   
                    </aside>
                </div>
                <!-- SIDE BAR END -->    
            </div>
        </div>
    </div>
</div>   

<?php
get_footer();
?>