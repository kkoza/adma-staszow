<?php
/**
 * The template for displaying 404 pages (Not Found)
 */
$indusza_redux_demo = get_option('redux_demo');
get_header(); ?> 

<div class="page-wraper">
    <?php if(isset($indusza_redux_demo['blog_image']['url']) && $indusza_redux_demo['blog_image']['url'] != ''){?>
        <div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo esc_url($indusza_redux_demo['blog_image']['url']);?>);">
        <?php }else{?> 
            <div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo (get_template_directory_uri().'/assets/images/banner/1.jpg');?>);">
            <?php } ?>
            <div class="overlay-main site-bg-black opacity-06"></div>
            <div class="container">
                <div class="wt-bnr-inr-entry">
                    <div class="banner-title-outer">
                        <div class="banner-title-name">
                            <h2 class="wt-title"><?php echo esc_html__( 'Error 404', 'indusza' );?></h2>
                        </div>
                    </div>
                    <!-- BREADCRUMB ROW -->  
                    <div>
                        <ul class="wt-breadcrumb breadcrumb-style-2">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>"><?php echo esc_html__( 'Home', 'indusza' )?></a></li>
                            <li><?php echo esc_html__( 'Error 404', 'indusza' );?></li>
                        </ul>
                    </div>
                    <!-- BREADCRUMB ROW END -->                        
                </div>
            </div>
        </div>
    <!-- CONTENT START -->
    <div class="page-content">
        <!-- Error SECTION START -->
        <div class="section-full page-notfound-outer">
            <div class="container-fluid">
                <div class="section-content">
                    <div class="page-notfound">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-12">
                                <div class="page-notfound-content">
                                    <h3 class="error-title"><?php if(isset($indusza_redux_demo['404_top_title']) && $indusza_redux_demo['404_top_title'] != ''){?>
                                            <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['404_top_title']));?>
                                        <?php }else{?>
                                            <?php echo esc_html__( 'Error', 'indusza' );
                                        }?></h3>
                                    <strong><?php if(isset($indusza_redux_demo['404_title']) && $indusza_redux_demo['404_title'] != ''){?>
                                        <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['404_title']));?>
                                    <?php }else{?>
                                        <?php echo esc_html__( '404', 'indusza' );
                                    }?></strong>
                                        <h3 class="error-comment"><?php if(isset($indusza_redux_demo['404_subtitle']) && $indusza_redux_demo['404_subtitle'] != ''){?>
                                            <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['404_subtitle']));?>
                                        <?php }else{?>
                                            <?php echo esc_html__( 'Oops! Looks like the page is gone.', 'indusza' );
                                        }?></h3>
                                        <p><?php if(isset($indusza_redux_demo['404_text']) && $indusza_redux_demo['404_text'] != ''){?>
                                            <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['404_text']));?>
                                        <?php }else{?>
                                            <?php echo esc_html__( 'The page you are looking for might have been removed had its name changed or is temporarily unavailable', 'indusza' );
                                        }?> </p>
                                        <a href="<?php echo esc_url(home_url('/')) ?>" class="site-button sb-bdr-dark"><?php if(isset($indusza_redux_demo['404_button']) && $indusza_redux_demo['404_button'] != ''){?>
                                            <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['404_button']));?>
                                        <?php }else{?>
                                            <?php echo esc_html__( 'Back to Home', 'indusza' );
                                        }?></a>
                                    </div>
                                </div>
                                <?php if(isset($indusza_redux_demo['404_image']['url']) && $indusza_redux_demo['404_image']['url'] != ''){?>
                                    <div class="col-xl-6 col-lg-6 col-md-12">                          
                                        <div class="page-notfound-media text-right">
                                            <img src="<?php echo esc_url($indusza_redux_demo['404_image']['url']);?>" alt="">
                                        </div>
                                    </div>     
                                <?php }else{?> 
                                    <div class="col-xl-6 col-lg-6 col-md-12">                          
                                        <div class="page-notfound-media text-right">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/images/error-bg.png" alt="">
                                        </div>
                                    </div> 
                                <?php } ?>
                            </div>                         
                        </div>
                    </div>
                </div>
            </div>   
            <!-- Error  SECTION END -->            
        </div>
        <!-- CONTENT END -->
    </div>

    <!-- LOADING AREA START ===== -->
    <div class="loading-area">
        <div class="loading-box"></div>
        <div class="loading-pic">
            <div class="cssload-spinner"></div>
        </div>
    </div>

    <?php get_footer(); ?>