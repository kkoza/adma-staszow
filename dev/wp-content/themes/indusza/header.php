<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<?php $indusza_redux_demo = get_option('redux_demo'); ?>

<head>

    <!-- Meta Tags -->

    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {?>

        <?php if(isset($indusza_redux_demo['favicon']['url'])){?>

            <link rel="shortcut icon" href="<?php echo esc_url($indusza_redux_demo['favicon']['url']); ?>">

        <?php } ?>

    <?php } ?>

    <?php wp_head(); ?> 


</head>

<body <?php body_class(); ?>>

 <div class="page-wraper">

    <!-- HEADER START -->
    <header class="site-header header-style-1 mobile-sider-drawer-menu">

        <div class="top-bar site-bg-white">
            <div class="container">

                <div class="d-flex justify-content-between">
                    <div class="wt-topbar-left d-flex flex-wrap align-content-start">
                        <ul class="wt-topbar-left-info">
                            <?php if(isset($indusza_redux_demo['header_phone'])){?>
                            <li><i class="flaticon-call"></i><span><?php echo esc_html__( 'Call :', 'indusza' )?></span> <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['header_phone']));?></li>
                            <?php } ?>
                            <?php if(isset($indusza_redux_demo['header_email'])){?>
                            <li><i class="flaticon-mail"></i><span><?php echo esc_html__( 'Email :', 'indusza' )?></span> <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['header_email']));?></li>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="wt-topbar-right d-flex flex-wrap align-content-center">
                        <div class="wt-topbar-right-info">
                            <ul class="social-icons">
                                <?php if(isset($indusza_redux_demo['header_link_facebook'])){?>
                                <li><a href="<?php echo esc_url($indusza_redux_demo['header_link_facebook']);?>" class="fa fa-facebook"></a></li>
                                <?php } ?>
                                <?php if(isset($indusza_redux_demo['header_link_twitter'])){?>
                                <li><a href="<?php echo esc_url($indusza_redux_demo['header_link_twitter']);?>" class="fa fa-twitter"></a></li>
                                <?php } ?>
                                <?php if(isset($indusza_redux_demo['header_link_linkedin'])){?>
                                <li><a href="<?php echo esc_url($indusza_redux_demo['header_link_linkedin']);?>" class="fa fa-linkedin"></a></li>
                                <?php } ?>
                                <?php if(isset($indusza_redux_demo['header_link_pinterest'])){?>
                                <li><a href="<?php echo esc_url($indusza_redux_demo['header_link_pinterest']);?>" class="fa fa-pinterest"></a></li>
                                <?php } ?>
                            </ul>
                        </div> 
                    </div>
                </div>


            </div>
        </div>  


        <div class="sticky-header main-bar-wraper  navbar-expand-lg">
            <div class="main-bar">  

                <div class="container clearfix"> 

                    <?php if(isset($indusza_redux_demo['logo']['url']) && $indusza_redux_demo['logo']['url'] != ''){?>
                        <div class="logo-header">
                            <div class="logo-header-inner logo-header-one">
                                <a href="<?php echo esc_url(home_url('/')); ?>">
                                    <img src="<?php echo esc_url($indusza_redux_demo['logo']['url']);?>" alt="">
                                </a>
                            </div>
                        </div>  
                    <?php }else{?>
                        <div class="logo-header">
                            <div class="logo-header-inner logo-header-one">
                                <a href="<?php echo esc_url(home_url('/')); ?>">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/images/logo-dark.png" alt="">
                                </a>
                            </div>
                        </div>  
                    <?php } ?>

                    <!-- NAV Toggle Button -->
                    <button id="mobile-side-drawer" data-target=".header-nav" data-toggle="collapse" type="button" class="navbar-toggler collapsed">
                        <span class="sr-only"><?php echo esc_html__( 'Toggle navigation', 'indusza' )?></span>
                        <span class="icon-bar icon-bar-first"></span>
                        <span class="icon-bar icon-bar-two"></span>
                        <span class="icon-bar icon-bar-three"></span>
                    </button> 

                    <!-- MAIN Vav -->
                    <div class="nav-animation header-nav navbar-collapse collapse d-flex justify-content-center">

                        <?php 

                        wp_nav_menu( 

                            array( 

                                'theme_location' => 'primary',

                                'container' => '',

                                'menu_class' => '', 

                                'menu_id' => '',

                                'menu'            => '',

                                'container_class' => '',

                                'container_id'    => '',

                                'echo'            => true,

                                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',

                                'walker'            => new indusza_wp_bootstrap_navwalker(),

                                'before'          => '',

                                'after'           => '',

                                'link_before'     => '',

                                'link_after'      => '',

                                'items_wrap'      => '<ul class=" nav navbar-nav%2$s">%3$s</ul>',

                                'depth'           => 0,        

                            )

                        ); ?>

                    </div>

                    <!-- Header Right Section-->
                    <div class="extra-nav header-1-nav">
                        <div class="extra-cell one">
                            <div class="header-search">
                                <a href="#search" class="header-search-icon"><i class="fa fa-search"></i></a>
                            </div>                                
                        </div>
                        <div class="extra-cell two">
                            <?php if(isset($indusza_redux_demo['header_button'])){?>
                            <div class="ap-btn-outer">
                                <a href="<?php echo esc_url($indusza_redux_demo['header_button_link']);?>" class="ap-btn"><?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['header_button']));?> <i class="fa fa-angle-double-right slide-right"></i></a>
                            </div>     
                            <?php } ?>                           
                        </div>                                
                    </div> 

                    <!-- Contact Nav -->     
                    <?php if(isset($indusza_redux_demo['side_panel_background']['url']) && $indusza_redux_demo['side_panel_background']['url'] != ''){?>
                        <div class="contact-slide-hide bg-cover" style="background-image:url(<?php echo esc_url($indusza_redux_demo['side_panel_background']['url']);?>);"> 
                    <?php }else{?>
                        <div class="contact-slide-hide bg-cover" style="background-image:url(<?php echo get_template_directory_uri();?>/assets/images/background/bg11.jpg);">
                    <?php } ?>
                        <div class="contact-nav">
                           <a href="javascript:void(0)" class="contact_close">&times;</a>
                           <div class="contact-nav-form p-a30">
                            <div class="contact-info   m-b30">

                                <?php if(isset($indusza_redux_demo['side_panel_phone'])){?>
                                <div class="wt-icon-box-wraper center p-b30">
                                    <div class="icon-xs m-b20 scale-in-center"><i class="fa fa-phone"></i></div>
                                    <div class="icon-content">
                                        <h4 class="m-t0  wt-title"><?php echo esc_html__( 'Phone number', 'indusza' )?></h4>
                                        <p><?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['side_panel_phone']));?></p>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($indusza_redux_demo['side_panel_email'])){?>
                                <div class="wt-icon-box-wraper center p-b30">
                                    <div class="icon-xs m-b20 scale-in-center"><i class="fa fa-envelope"></i></div>
                                    <div class="icon-content">
                                        <h4 class="m-t0 wt-title"><?php echo esc_html__( 'Email address', 'indusza' )?></h4>
                                        <p><?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['side_panel_email']));?></p>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if(isset($indusza_redux_demo['side_panel_address'])){?>
                                <div class="wt-icon-box-wraper center p-b30">
                                    <div class="icon-xs m-b20 scale-in-center"><i class="fa fa-map-marker"></i></div>
                                    <div class="icon-content">
                                        <h4 class="m-t0  wt-title"><?php echo esc_html__( 'Address info', 'indusza' )?></h4>
                                        <p><?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['side_panel_address']));?></p>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>               
                             <?php if(isset($indusza_redux_demo['side_panel_copyright'])){?>
                            <div class="text-center">
                                <span><?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['side_panel_copyright']));?></span> 
                            </div>   
                            <?php } ?>                                                                    
                        </div>
                    </div> 
                </div>
                <!-- SITE Search -->
                <div id="search"> 
                    <span class="close"></span>
                    <form role="search" id="searchform" action="<?php echo esc_url(home_url('/')); ?>" method="get" class="radius-xl">
                        <div class="input-group">
                            <input class="form-control" name="s" type="search" placeholder="<?php echo esc_html__( 'Type to search', 'indusza' )?>"/>
                            <span class="input-group-append"><button type="submit" class="search-btn"><i class="fa fa-search"></i></button></span>
                        </div> 
                    </form>
                </div> 
            </div>    
        </div>
    </div>
</header>
<!-- HEADER END -->

<!-- CONTENT START -->
<div class="page-content">