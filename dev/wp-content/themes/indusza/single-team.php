<?php
/**
 * The Template for displaying all single posts
 */
$indusza_redux_demo = get_option('redux_demo');
get_header(); ?>

<?php 
while (have_posts()): the_post();
    $team_position = get_post_meta(get_the_ID(),'_cmb_team_position', true);
    ?>

    <?php if(isset($indusza_redux_demo['blog_image']['url']) && $indusza_redux_demo['blog_image']['url'] != ''){?>
        <div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo esc_url($indusza_redux_demo['blog_image']['url']);?>);">
        <?php }else{?> 
            <div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo get_template_directory_uri();?>/assets/images/banner/1.jpg);">
            <?php } ?>
            <div class="overlay-main site-bg-black opacity-06"></div>
            <div class="container">
                <div class="wt-bnr-inr-entry">
                    <div class="banner-title-outer">
                        <div class="banner-title-name">
                            <h2 class="wt-title"><?php if(isset($indusza_redux_demo['team_details_title']) && $indusza_redux_demo['team_details_title'] != ''){?>
                                <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['team_details_title']));?>
                            <?php }else{?>
                                <?php echo esc_html__( 'Team Detail', 'indusza' );
                            }
                            ?></h2>
                        </div>
                    </div>
                    <!-- BREADCRUMB ROW -->                            

                    <div>
                        <ul class="wt-breadcrumb breadcrumb-style-2">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>"><?php echo esc_html__( 'Home', 'indusza' )?></a></li>
                            <li><?php if(isset($indusza_redux_demo['team_details_title']) && $indusza_redux_demo['team_details_title'] != ''){?>
                                <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['team_details_title']));?>
                            <?php }else{?>
                                <?php echo esc_html__( 'Team Detail', 'indusza' );
                            }
                            ?></li>
                        </ul>
                    </div>

                    <!-- BREADCRUMB ROW END -->                        
                </div>
            </div>
        </div>
        <!-- INNER PAGE BANNER END -->


        <!-- OUR TEAM START -->
        <div class="section-full p-t120 p-b90  site-bg-white">

            <div class="container">


                <div class="section-content team_details_area">
                    <div class="row justify-content-center">
                        <div class="col-lg-5 col-md-6 m-b30">
                            <div class="wt-team-1">

                                <div class="wt-media">
                                    <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id());?>" alt="">
                                </div> 

                            </div>
                        </div>

                        <div class="col-lg-6 offset-lg-1 col-md-6 m-b30 team-box-single site-bg-gray-light">

                            <div class="team-detail">
                                <span class="team-position"><?php echo esc_attr($team_position); ?></span>                                        
                                <h4 class="m-t0 team-name"><a href="javascript:;"><?php the_title();?></a></h4>

                                <?php the_content(); ?>
                                <?php wp_link_pages(); ?>                                            
                            </div>

                        </div>

                    </div>
                </div>      

            </div>
        </div>   
        <!-- OUR TEAM SECTION END -->


        <!-- BUILD FUTURE START -->
        <div class="section-full site-bg-gray-light p-t120 p-b90 ">

            <div class="container">

                <div class="team-deatail-featured">
                    <h3 class="wt-title m-b30">Featured skills</h3>
                    <div class="row">

                        <div class="col-lg-6 col-md-6 m-b30">
                            <div class="our-future">
                                <span class="progressText "><B>Industry</B></span>
                                <div class="progress">
                                    <div class="progress-bar site-bg-primary progress-bar-striped progress-bar-animated" role="progressbar" style="width: 92%;" aria-valuenow="92" aria-valuemin="0" aria-valuemax="100"></div><span> 80%</span>
                                </div>                                            


                                <span class="progressText "><B>Construction</B></span>
                                <div class="progress">
                                    <div class="progress-bar site-bg-primary progress-bar-striped progress-bar-animated" role="progressbar" style="width: 84%;" aria-valuenow="84" aria-valuemin="0" aria-valuemax="100"></div><span> 90%</span>
                                </div>

                                <span class="progressText "><B>Factory</B></span>
                                <div class="progress">
                                    <div class="progress-bar site-bg-primary progress-bar-striped progress-bar-animated" role="progressbar" style="width: 72%;" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div><span>95%</span>
                                </div>

                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 m-b30">
                            <div class="our-future">
                                <span class="progressText "><B>Mastery of construction machinery</B></span>
                                <div class="progress">
                                    <div class="progress-bar site-bg-primary progress-bar-striped progress-bar-animated" role="progressbar" style="width: 92%;" aria-valuenow="92" aria-valuemin="0" aria-valuemax="100"></div><span> 80%</span>
                                </div>                                            


                                <span class="progressText "><B>Builders planning</B></span>
                                <div class="progress">
                                    <div class="progress-bar site-bg-primary progress-bar-striped progress-bar-animated" role="progressbar" style="width: 84%;" aria-valuenow="84" aria-valuemin="0" aria-valuemax="100"></div><span> 90%</span>
                                </div>

                                <span class="progressText "><B>Construction and design</B></span>
                                <div class="progress">
                                    <div class="progress-bar site-bg-primary progress-bar-striped progress-bar-animated" role="progressbar" style="width: 72%;" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div><span>95%</span>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>  

            </div>

        </div>   
        <!-- BUILD FUTURE END -->         
    <?php endwhile; ?>
    <!-- OUR TEAM START -->
    <div class="section-full p-t120 p-b90 site-bg-white">

        <div class="container">
            <!-- TITLE START-->
            <div class="section-head center wt-small-separator-outer">
                <div class="wt-small-separator site-text-primary">
                    <div class="sep-leaf-left"></div>
                    <div>Our Team</div>
                </div>
                <h2 class="wt-title">Our Experts</h2>
            </div>
            <!-- TITLE END-->

            <div class="section-content">
                <div class="team3-outer">

                    <div class="row justify-content-center">
                        <?php 
                        $args = array(   
                            'post_type' => 'team',
                            'posts_per_page' => 6,
                        );  
                        $wp_query = new WP_Query($args);
                        while ($wp_query -> have_posts()) : $wp_query -> the_post();
                            $team_image = get_post_meta(get_the_ID(),'_cmb_team_image', true);
                            $team_position = get_post_meta(get_the_ID(),'_cmb_team_position', true);
                            ?>
                            <div class="col-lg-4 col-md-6 col-sm-12 m-b30">
                                <div class="wt-team-3">

                                    <div class="wt-media">
                                        <img src="<?php echo wp_get_attachment_url($team_image);?>" alt="">
                                    </div>                                   

                                    <div class="wt-info">
                                        <div class="team-detail">
                                            <h4 class="m-t0 team-name"><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>                                            
                                            <span class="team-position"><?php echo esc_attr($team_position); ?></span>                                        
                                        </div>
                                        <div class="team-social-center">
                                            <ul class="team-social-bar">
                                                <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="http://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="https://www.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
                                                <li><a href="http://pinterest.com/"><i class="fa fa-pinterest"></i></a></li>
                                            </ul>
                                        </div>                                        
                                    </div>

                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>      
        </div>
    </div>  

    <?php
    get_footer();
    ?>