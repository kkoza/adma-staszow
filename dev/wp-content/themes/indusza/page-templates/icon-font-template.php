<?php
/*
 * Template Name: Icon Font
 * Description: A Page Template with a Page Builder design.
 */
 $indusza_redux_demo = get_option('redux_demo');
get_header(); ?>


<div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo get_template_directory_uri();?>/assets/images/banner/1.jpg);">
    <div class="overlay-main site-bg-black opacity-06"></div>
    <div class="container">
        <div class="wt-bnr-inr-entry">
            <div class="banner-title-outer">
                <div class="banner-title-name">
                    <h2 class="wt-title">Font Icon</h2>
                </div>
            </div>
            <!-- BREADCRUMB ROW -->                            
            
                <div>
                    <ul class="wt-breadcrumb breadcrumb-style-2">
                        <li><a href="index.html">Home</a></li>
                        <li>Font Icon</li>
                    </ul>
                </div>
            
            <!-- BREADCRUMB ROW END -->                        
        </div>
    </div>
</div>
<!-- INNER PAGE BANNER END -->
    
    
    <div class="section-full p-t120 p-b90">
        <div class="container">
            <div class="section-content">
                <div class="wt-box">
                    <div class="icon-font row d-flex justify-content-center flex-wrap">
                    
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-checked"></div>
                                    <div class="class-name">.flaticon-checked</div>
                                </div>        
                            </div> 
                              
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-right"></div>
                                    <div class="class-name">.flaticon-right</div>
                                </div>        
                            </div> 
                              
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-left"></div>
                                    <div class="class-name">.flaticon-left</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-chat"></div>
                                    <div class="class-name">.flaticon-chat</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-lightbulb"></div>
                                    <div class="class-name">.flaticon-lightbulb</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-antivirus"></div>
                                    <div class="class-name">.flaticon-antivirus</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-customer-service"></div>
                                    <div class="class-name">.flaticon-customer-service</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-physics"></div>
                                    <div class="class-name">.flaticon-physics</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-graphic-tool"></div>
                                    <div class="class-name">.flaticon-graphic-tool</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-gas-station"></div>
                                    <div class="class-name">.flaticon-gas-station</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-quote-1"></div>
                                    <div class="class-name">.flaticon-quote-1</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-quote"></div>
                                    <div class="class-name">.flaticon-quote</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-email"></div>
                                    <div class="class-name">.flaticon-email</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-call"></div>
                                    <div class="class-name">.flaticon-call</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-location"></div>
                                    <div class="class-name">.flaticon-location</div>
                                </div>        
                            </div>  
                               
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-mail"></div>
                                    <div class="class-name">.flaticon-mail</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-worker"></div>
                                    <div class="class-name">.flaticon-worker</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-briefing"></div>
                                    <div class="class-name">.flaticon-briefing</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-factory"></div>
                                    <div class="class-name">.flaticon-factory</div>
                                </div>        
                            </div>   
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-reliability"></div>
                                    <div class="class-name">.flaticon-reliability</div>
                                </div>        
                            </div>
                            
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-oil"></div>
                                    <div class="class-name">.flaticon-oil</div>
                                </div>        
                            </div> 
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-helmet"></div>
                                    <div class="class-name">.flaticon-helmet</div>
                                </div>        
                            </div> 
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-work-time"></div>
                                    <div class="class-name">.flaticon-work-time</div>
                                </div>        
                            </div> 
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-plant"></div>
                                    <div class="class-name">.flaticon-plant</div>
                                </div>        
                            </div>  
                            
                            <div class="col-md-3 col-sm-6 m-b30">
                                <div class="icon-font-block"><div class="flat-icon flaticon-car-parts"></div>
                                    <div class="class-name">.flaticon-car-parts</div>
                                </div>        
                            </div> 
                                                               
                            
                        </div>
                </div>
            </div>
        </div>
    </div> 

            
<?php get_footer(); ?>