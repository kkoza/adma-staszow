<?php

/*

 * Template Name: Blog Grid

 * Description: A Page Template with a Page Builder design.

 */

$indusza_redux_demo = get_option('redux_demo');

get_header(); ?>

<?php if(isset($indusza_redux_demo['blog_image']['url']) && $indusza_redux_demo['blog_image']['url'] != ''){?>
<div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo esc_url($indusza_redux_demo['blog_image']['url']);?>);">
<?php }else{?> 
<div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo (get_template_directory_uri().'/assets/images/banner/1.jpg');?>);">
<?php } ?>
    <div class="overlay-main site-bg-black opacity-06"></div>
    <div class="container">
        <div class="wt-bnr-inr-entry">
            <div class="banner-title-outer">
                <div class="banner-title-name">
                    <h2 class="wt-title"><?php if(isset($indusza_redux_demo['blog_grid_title']) && $indusza_redux_demo['blog_grid_title'] != ''){?>
                        <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['blog_grid_title']));?>
                    <?php }else{?>
                        <?php echo esc_html__( 'Blog Grid', 'indusza' );
                    }
                    ?></h2>
                </div>
            </div>
            <!-- BREADCRUMB ROW -->                            

            <div>
                <ul class="wt-breadcrumb breadcrumb-style-2">
                    <li><a href="<?php echo esc_url(home_url('/')); ?>"><?php echo esc_html__( 'Home', 'indusza' )?></a></li>
                    <li><?php if(isset($indusza_redux_demo['blog_grid_title']) && $indusza_redux_demo['blog_grid_title'] != ''){?>
                        <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['blog_grid_title']));?>
                    <?php }else{?>
                        <?php echo esc_html__( 'Blog Grid', 'indusza' );
                    }
                    ?></li>
                </ul>
            </div>
            <!-- BREADCRUMB ROW END -->                        
        </div>
    </div>
</div>
<!-- INNER PAGE BANNER END -->


<!-- OUR BLOG START -->
<div class="section-full p-t120  p-b90 bg-white">
    <div class="container">
        <div class="section-head center wt-small-separator-outer">
            <div class="wt-small-separator site-text-primary">
                <div class="sep-leaf-left"></div>
                <div><?php if(isset($indusza_redux_demo['blog_grid_heading']) && $indusza_redux_demo['blog_grid_heading'] != ''){?>
                        <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['blog_grid_heading']));?>
                    <?php }else{?>
                        <?php echo esc_html__( 'News And Updates', 'indusza' );
                    }
                    ?></div>                                
            </div>
            <h2 class="wt-title"><?php if(isset($indusza_redux_demo['blog_grid_sub_heading']) && $indusza_redux_demo['blog_grid_sub_heading'] != ''){?>
                <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['blog_grid_sub_heading']));?>
            <?php }else{?>
                <?php echo esc_html__( 'Lets Checkout Our All Current News.', 'indusza' );
            }
            ?></h2>
        </div>
    </div>

    <div class="container">
        <div class="section-content">
            <div class="row d-flex justify-content-center">
                <?php 
                $args = array(   
                    'post_type' => 'post', 
                    'posts_per_page' => 6,
                );  
                $wp_query = new WP_Query($args);
                while ($wp_query -> have_posts()) : $wp_query -> the_post();
                $column_image = get_post_meta(get_the_ID(),'_cmb_column_image', true);
                    ?>
                <div class="col-lg-4 col-md-6 col-sm-12 m-b30">
                    <!--Block one-->
                    <div class="blog-post blog-post-4-outer">
                        <?php if (has_post_thumbnail()) { ?>
                        <div class="wt-post-media wt-img-effect zoom-slow">
                            <a href="<?php the_permalink();?>"><img src="<?php echo wp_get_attachment_url($column_image);?>" alt=""></a>
                        </div>      
                        <?php } ?>                             
                        <div class="wt-post-info">
                            <div class="wt-post-meta ">
                                <ul>
                                    <li class="post-author"><i class="fa fa-user"></i> <?php echo esc_html__( 'By', 'indusza' )?>  <?php the_author_posts_link(); ?></li>
                                    <li class="post-comment"><i class="fa fa-comments"></i> <?php comments_number( esc_html__('0 Comments', 'indusza'), esc_html__('1 Comment', 'indusza'), esc_html__('% Comments', 'indusza')); ?></li>
                                </ul>
                            </div>                                 
                            <div class="wt-post-title ">
                                <h4 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
                            </div>

                            <div class="wt-post-text ">
                                <p><?php if(isset($indusza_redux_demo['blog_excerpt_2'])){?>
                                    <?php echo esc_attr(indusza_excerpt_2($indusza_redux_demo['blog_excerpt_2'])); ?>
                                <?php }else{?>
                                    <?php echo esc_attr(indusza_excerpt_2(15)); 
                                }
                                ?></p>
                            </div>  

                            <div class="wt-post-readmore ">
                                <a href="<?php the_permalink();?>" class="site-button-link black"><?php echo esc_html__( 'Read More', 'indusza' );?></a>
                            </div>                                        
                        </div>                                
                    </div>
                </div>
            <?php endwhile; ?> 
            </div>
        </div>
    </div>
</div>   
<!-- OUR BLOG END -->


<?php
get_footer();
?>