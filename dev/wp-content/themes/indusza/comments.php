<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains comments and the comment form.
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() )
    return;
?>

  <?php if ( have_comments() ) : ?>
            
      <h3 class="comments-title m-t0"><?php comments_number( esc_html__('0 Comments', 'indusza'), esc_html__('1 Comment', 'indusza'), esc_html__('% Comments', 'indusza')); ?></h3>
      <div>
        <ol class="comment-list">
        <?php wp_list_comments('callback=indusza_theme_comment'); ?>
        </ol>
      </div>
                                   
    <?php
            // Are there comments to navigate through?
    if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
      ?>
    <div class="text-center">
      <ul class="pagination">
        <li>
          <?php //Create pagination links for the comments on the current post, with single arrow heads for previous/next
          paginate_comments_links( array(
            'prev_text' => wp_specialchars_decode(esc_html__( '<i class="fa fa-angle-left"></i>', 'indusza' ),ENT_QUOTES), 
            'next_text' => wp_specialchars_decode(esc_html__( '<i class="fa fa-angle-right"></i>', 'indusza' ),ENT_QUOTES)
          ));  ?>
        </li> 
      </ul>
    </div>
<?php endif; // Check for comment navigation ?>
<?php endif; ?>
<?php
    if ( is_singular() ) wp_enqueue_script( "comment-reply" );
        $aria_req = ( $req ? " aria-required='true'" : '' );
        $comment_args = array(
                'id_form' => 'commentform',        
                'class_form' => 'comment-form',                         
                'title_reply'=> esc_html__( 'Leave a comment', 'indusza' ),
                'fields' => apply_filters( 'comment_form_default_fields', array(
                    'author' => '<p class="comment-form-author">
                                    <label>'.esc_attr__('Name', 'indusza').'<span class="required">*</span></label>
                                    <input type="text" class="form-control" placeholder="'.esc_attr__('Author', 'indusza').'" name="author" id="author">
                                  </p>',
                    'email' => '<p class="comment-form-email">
                                   <label>'.esc_attr__('Email', 'indusza').'<span class="required">*</span></label>
                                  <input type="email" class="form-control" placeholder="'.esc_attr__('Email', 'indusza').'" name="email">
                                </p>',
                    'website' => '<p class="comment-form-url">
                                   <label>'.esc_attr__('Website', 'indusza').'</label>
                                  <input type="text" class="form-control" placeholder="'.esc_attr__('Website', 'indusza').'" name="url">
                                </p>'
                ) ),
                'comment_field' => '<p class="comment-form-comment">
                                      <label>'.esc_attr__('Comment', 'indusza').'</label>
                                        <textarea name="comment" id="comment" class="form-control" placeholder="'.esc_attr__('Write a Comment...', 'indusza').'"></textarea>
                                    </p>',                    
                 'label_submit' => esc_attr__( 'Post a Comment', 'indusza' ),
                 'class_submit' => 'site-button site-btn-effect',
                 'comment_notes_before' => '',
                 'comment_notes_after' => '',               
        )
    ?>
<?php if ( comments_open() ) : ?>
    <?php comment_form($comment_args); ?>
<?php endif; ?>