<?php
/**
 * The Template for displaying all single posts
 */
$indusza_redux_demo = get_option('redux_demo');
get_header(); ?>

<?php 
while (have_posts()): the_post();
    $postid = get_the_ID();
    ?>

    <?php if(isset($indusza_redux_demo['blog_image']['url']) && $indusza_redux_demo['blog_image']['url'] != ''){?>
        <div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo esc_url($indusza_redux_demo['blog_image']['url']);?>);">
        <?php }else{?> 
            <div class="wt-bnr-inr overlay-wraper bg-center" style="background-image:url(<?php echo get_template_directory_uri();?>/assets/images/banner/1.jpg);">
            <?php } ?>
            <div class="overlay-main site-bg-black opacity-06"></div>
            <div class="container">
                <div class="wt-bnr-inr-entry">
                    <div class="banner-title-outer">
                        <div class="banner-title-name">
                            <h2 class="wt-title"><?php if(isset($indusza_redux_demo['service_details_title']) && $indusza_redux_demo['service_details_title'] != ''){?>
                                <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['service_details_title']));?>
                            <?php }else{?>
                                <?php echo esc_html__( 'Service Detail', 'indusza' );
                            }
                            ?></h2>
                        </div>
                    </div>
                    <!-- BREADCRUMB ROW -->

                    <div>
                        <ul class="wt-breadcrumb breadcrumb-style-2">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>"><?php echo esc_html__( 'Home', 'indusza' )?></a></li>
                            <li><?php if(isset($indusza_redux_demo['service_details_title']) && $indusza_redux_demo['service_details_title'] != ''){?>
                                <?php echo htmlspecialchars_decode(esc_attr($indusza_redux_demo['service_details_title']));?>
                            <?php }else{?>
                                <?php echo esc_html__( 'Service Detail', 'indusza' );
                            }
                            ?></li>
                        </ul>
                    </div>

                    <!-- BREADCRUMB ROW END -->
                </div>
            </div>
        </div>
        <!-- INNER PAGE BANNER END -->         

        <!-- SERVICE DETAIL SECTION START -->
        <div class="section-full p-t120 p-b90 bg-white">
            <div class="section-content">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-3 col-md-12 rightSidebar">
                            <?php get_sidebar('services');?>
                        </div>
                    <?php endwhile; ?> 
                    <?php while (have_posts()): the_post();
                        $postid = get_the_ID();
                        ?>
                        <div class="col-lg-9 col-md-12">
                            <div class="section-content service-full-info">

                                <div class="services-etc m-b30">
                                    <div class="wt-media m-b30">
                                        <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id());?>" alt=""> 
                                    </div>                           
                                    <div class="text-left">
                                        <h4 class="wt-title m-b20"><?php the_title();?></h4>
                                    </div>
                                    <?php the_content(); ?>
                                    <?php wp_link_pages(); ?>  
                                </div>
                            </div>
                        </div>
                    </div>                            
                </div>                                 
            </div>
        </div>  
    <?php endwhile; ?> 
    <?php
    get_footer();
    ?>