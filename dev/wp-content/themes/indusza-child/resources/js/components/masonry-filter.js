/**
 * If are you using link e.g https://example.com/examples/#custom-filter, script will be toggle automaticly related tab.
 */
export default class MasonryFilter {
    init = () => {
        window.addEventListener('load', this.run);
        window.addEventListener('hashchange', this.run);
    }
    run = () => {
        let hash = this.getHash();
        const link = this.getLink(hash);
        if (!link) {
            return;
        }
        const parent = link.parentElement;

        this.toggleTab(parent);
    }

    /**
     * If the navElement is an instance of HTMLElement, then wait one second and click it.
     * @param navElement - The element that you want to click on.
     * @returns false.
     */
    toggleTab = (navElement) => {
        if (!(navElement instanceof HTMLElement)) {
            return false;
        }
        setTimeout(function () {
            navElement.click();
        }, 1000);
    }

    /**
     * Get the link that has the same data-filter attribute as the hash in the URL.
     * @param hash - The hash of the current URL.
     * @returns The link that has the data-filter attribute with the value of the hash.
     */
    getLink = (hash) => {
        return document.querySelector(`[data-filter=".${hash}"]`);
    }

    /**
     * It gets the hash from the URL and returns it.
     * @returns The hash value of the current URL.
     */
    getHash = () => {
        let hash = window.location.hash;
        hash = hash.replace('#', '');
        return hash;
    }
}