<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class Home1ClientCustom extends \BdevsElementor\Widget\Home1Client {

	protected function _register_controls() {
		$this->start_controls_section(
			'section_client',
			[
				'label' => esc_html__( 'Client Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'image',
			[
				'label'   => esc_html__( 'Client Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::MEDIA,
				'dynamic' => [ 'active' => true ],
				'description' => esc_html__( 'Add Your Client Image', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'top_title',
			[
				'label'       => __( 'Top Title', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Top Title', 'bdevs-elementor' ),
				'default'     => __( 'It is Top Title', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'tabs_1',
			[
				'label' => esc_html__( 'Logo Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [	
					[
						'name'    => 'tab_image',
						'label'   => esc_html__( 'Image', 'bdevs-elementor' ),
						'type'    => Controls_Manager::MEDIA,
						'dynamic' => [ 'active' => true ],
					],
				],
			]
		);

		$this->add_control(
			'tabs_2',
			[
				'label' => esc_html__( 'Help Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [	
					[
						'name'        => 'help_select',
						'label'     => esc_html__( 'Help Select', 'bdevs-elementor' ),
						'type'      => Controls_Manager::SELECT,
						'dynamic' => [ 'active' => true ],
						'options'   => [
							'1'  => esc_html__( 'Type 1', 'bdevs-elementor' ),
							'2'  => esc_html__( 'Type 2', 'bdevs-elementor' ),
							'3'  => esc_html__( 'Type 3', 'bdevs-elementor' ),
						],
						'default'   => '1',
					],
					[
						'name'        => 'tab_icon',
						'label'       => esc_html__( 'Icon', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true,
					],
					[
						'name'        => 'tab_title',
						'label'       => esc_html__( 'Title', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( 'Tab Title' , 'bdevs-elementor' ),
						'label_block' => true,
					],
					[
						'name'        => 'tab_subtitle',
						'label'       => esc_html__( 'Subtitle', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( 'Tab Subtitle' , 'bdevs-elementor' ),
						'label_block' => true,
					],
				],
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_top_title',
			[
				'label'   => esc_html__( 'Show Top title', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_image',
			[
				'label'   => esc_html__( 'Show Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

        $this->add_control(
			'show_logo_items',
			[
				'label'   => esc_html__( 'Show Logo Items', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

        $this->add_control(
			'show_help_items',
			[
				'label'   => esc_html__( 'Show Help Items', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>
		<?php if (( '' !== $settings['image']['url'] ) && ( $settings['show_image'] )): ?>
		<div class="section-full site-bg-gray-light p-t120 p-b120 bg-cover" style="background-image:url(<?php echo wp_kses_post($settings['image']['url']); ?>);">
		<?php endif; ?>
			<div class="container home-client-outer">
				<!-- TITLE START-->
				<div class="section-head center wt-small-separator-outer">
					<?php if (( '' !== $settings['top_title'] ) && ( $settings['show_top_title'] )): ?>
					<div class="wt-small-separator site-text-primary">
						<div  class="sep-leaf-left"></div>
						<div><?php echo wp_kses_post($settings['top_title']); ?></div>                                
					</div>
					<?php endif; ?>
					<?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
					<h2><?php echo wp_kses_post($settings['heading']); ?></h2>
					<?php endif; ?>
				</div>
				<!-- TITLE END-->
				<div class="section-content">
                    <?php if (( '' !== $settings['tabs_1'] ) && ( $settings['show_logo_items'] )): ?>
                        <div class="owl-carousel home-client-carousel owl-btn-vertical-center">
                            <?php foreach ( $settings['tabs_1'] as $item ) : ?>
                                <div class="item">
                                    <div class="ow-client-logo">
                                        <?php if ( '' !== $item['tab_image']['url'] ) : ?>
                                        <div class="client-logo client-logo-media">
                                            <a href="javascript:void(0);"><img src="<?php echo wp_kses_post($item['tab_image']['url']); ?>" alt=""></a></div>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
				</div>
				<div class="container">
                    <?php if (( '' !== $settings['tabs_2'] ) && ( $settings['show_help_items'] )): ?>
					<div class="help-blocks m-t50 m-b30">
						<div class="row justify-content-center no-gutters">
							<?php foreach ( $settings['tabs_2'] as $item ) : ?>
								<?php if( wp_kses_post($item['help_select']) == '1'): ?>
								<div class="col-lg-4 col-md-6 site-bg-primary">
									<div class="help-block-content white">
										<div class="wt-icon-box-wraper left">
											<?php if ( '' !== $item['tab_icon'] ) : ?>
											<div class="wt-icon-box-sm">
												<span class="icon-cell site-bg-dark"><i class="<?php echo wp_kses_post($item['tab_icon']); ?>"></i></span>
											</div>
											<?php endif; ?>
											<div class="icon-content">
												<?php if ( '' !== $item['tab_title'] ) : ?>
												<h4 class="wt-tilte"><?php echo wp_kses_post($item['tab_title']); ?></h4>
												<?php endif; ?>
												<?php if ( '' !== $item['tab_subtitle'] ) : ?>
												<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
												<?php endif; ?>
											</div>
										</div>
									</div>
								</div>
								<?php elseif( wp_kses_post($item['help_select']) == '2'): ?>
									<div class="col-lg-4 col-md-6 site-bg-gray">
										<div class="help-block-content ">
											<div class="wt-icon-box-wraper left">
												<?php if ( '' !== $item['tab_icon'] ) : ?>
													<div class="wt-icon-box-sm">
														<span class="icon-cell site-bg-dark"><i class="<?php echo wp_kses_post($item['tab_icon']); ?>"></i></span>
													</div>
												<?php endif; ?>
												<div class="icon-content">
													<?php if ( '' !== $item['tab_title'] ) : ?>
														<h4 class="wt-tilte"><?php echo wp_kses_post($item['tab_title']); ?></h4>
													<?php endif; ?>
													<?php if ( '' !== $item['tab_subtitle'] ) : ?>
														<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
													<?php endif; ?>
												</div>
											</div>
										</div>
									</div>
								<?php elseif( wp_kses_post($item['help_select']) == '3'): ?>
									<div class="col-lg-4 col-md-6  site-bg-black">
										<div class="help-block-content white">
											<div class="wt-icon-box-wraper left">
												<?php if ( '' !== $item['tab_icon'] ) : ?>
												<div class="wt-icon-box-sm">
													<span class="icon-cell site-bg-dark"><i class="<?php echo wp_kses_post($item['tab_icon']); ?>"></i></span>
												</div>
												<?php endif; ?>
												<div class="icon-content">
													<?php if ( '' !== $item['tab_title'] ) : ?>
														<h4 class="wt-tilte"><?php echo wp_kses_post($item['tab_title']); ?></h4>
													<?php endif; ?>
													<?php if ( '' !== $item['tab_subtitle'] ) : ?>
														<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
													<?php endif; ?>
												</div>
											</div>
										</div>
									</div> 
								<?php endif; ?> 
							<?php endforeach; ?>
						</div>
					</div>
                    <?php endif; ?>
				</div>
			</div>

	<?php
	}
}