<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class Home1ProgressCustom extends \BdevsElementor\Widget\Home1Progress {

	protected function _register_controls() {

		$this->start_controls_section(
			'section_progress',
			[
				'label' => esc_html__( 'Progress Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'image',
			[
				'label'   => esc_html__( 'Progress Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::MEDIA,
				'dynamic' => [ 'active' => true ],
				'description' => esc_html__( 'Add Your Progress Image', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'top_title',
			[
				'label'       => __( 'Top Title', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Top Title', 'bdevs-elementor' ),
				'default'     => __( 'It is Top Title', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'link',
			[
				'label'       => __( 'Link', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Link', 'bdevs-elementor' ),
				'default'     => __( 'It is Link', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'button',
			[
				'label'       => __( 'Button', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Button', 'bdevs-elementor' ),
				'default'     => __( 'It is Button', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'button-link',
			[
				'label'       => __( 'Button link', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Link', 'bdevs-elementor' ),
				'default'     => __( 'It is Link', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);
		
		$this->add_control(
			'tabs',
			[
				'label' => esc_html__( 'About Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [	
					[
						'name'        => 'tab_title',
						'label'       => esc_html__( 'Tab Title', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_percent',
						'label'       => esc_html__( 'Tab Percent', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_value',
						'label'       => esc_html__( 'Tab Value', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_top_title',
			[
				'label'   => esc_html__( 'Show Top Title', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_image',
			[
				'label'   => esc_html__( 'Show Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		// custom
		$this->add_control(
			'show_content',
			[
				'label'   => esc_html__( 'Show Content', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_button',
			[
				'label'   => esc_html__( 'Show Button', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);
		//

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>
		<div class="section-full bg-white">
			<div class="container-fluid future-section-outer ">
				<div class="row">
					<?php if (( '' !== $settings['image']['url'] ) && ( $settings['show_image'] )): ?>
					<div class="col-xl-6 col-lg-6 future-section-left bg-cover" style="background-image:url(<?php echo wp_kses_post($settings['image']['url']); ?>);">
					</div>
					<?php endif; ?>
					<div class="col-xl-6 col-lg-6 future-section-right bg-white">
						<div class="future-sec-right-content">
							<div class="future-right-inner">
								<div class="section-head left wt-small-separator-outer">
									<?php if (( '' !== $settings['top_title'] ) && ( $settings['show_top_title'] )): ?>
									<div class="wt-small-separator site-text-primary">
										<div class="sep-leaf-left"></div>
										<div><?php echo wp_kses_post($settings['top_title']); ?></div>
									</div>
									<?php endif; ?>
									<?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
									<h2 class="wt-title"><?php echo wp_kses_post($settings['heading']); ?></h2>
									<?php endif; ?>
								</div>

								<div class="our-future">
									<?php foreach ( $settings['tabs'] as $item ) : ?>
										<span class="progressText "><B><?php echo wp_kses_post($item['tab_title']); ?></B></span>
										<div class="progress">
											<div class="progress-bar site-bg-primary progress-bar-striped progress-bar-animated" role="progressbar" style="width: <?php if ('' !== $item['tab_percent']): echo wp_kses_post($item['tab_percent']); else: echo 50; endif; ?>%;" aria-valuenow="<?php echo wp_kses_post($item['tab_value']); ?>" aria-valuemin="0" aria-valuemax="100"></div><span> <?php if ('' !== $item['tab_percent']): echo wp_kses_post($item['tab_percent']); else: echo 50; endif; ?></span>
										</div>                                            
									<?php endforeach; ?>
									<!-- custom home 1 button in progress widget-->
									<div class="our-future--button">
										<?php if (( '' !== $settings['button'] ) && ( $settings['show_button'] )): ?>
											<a href="<?php echo wp_kses_post($settings['button-link']); ?>" class="site-button sb-bdr-dark"><?php echo wp_kses_post($settings['button']); ?></a>  
										<?php endif; ?>
									</div>
									<!--  -->
								</div>  
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>   

	<?php
	}
}
