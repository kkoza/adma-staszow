<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class PageContactCustom extends \BdevsElementor\Widget\PageContact {

	protected function _register_controls() {
		$this->start_controls_section(
			'section_contact',
			[
				'label' => esc_html__( 'Contact Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'contact_form',
			[
				'label'       => __( 'Contact Form', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Contact Form', 'bdevs-elementor' ),
				'default'     => __( 'It is Contact Form', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'contact_image',
			[
				'label'       => esc_html__( 'Image', 'bdevs-elementor' ),
				'type'        => Controls_Manager::MEDIA,
				'description' => esc_html__( 'Add your Image', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'bg_image',
			[
				'label'   => esc_html__( 'Background Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::MEDIA,
				'dynamic' => [ 'active' => true ],
				'description' => esc_html__( 'Add Your Background Image', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => esc_html__( 'Address Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [			
					[
						'name'        => 'tab_title',
						'label'       => esc_html__( 'Title', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( 'Tab Title' , 'bdevs-elementor' ),
						'label_block' => true,
					],
					[
						'name'        => 'tab_subtitle',
						'label'       => esc_html__( 'Subtitle', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( 'Tab Subtitle' , 'bdevs-elementor' ),
						'label_block' => true,
					],
					[
						'name'        => 'tab_tel_link',
						'label'       => esc_html__( 'Tel. Number Link', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'label_block' => true,
					],
					[
						'name'        => 'tab_mail_link',
						'label'       => esc_html__( 'E-mail Link', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [ 'active' => true ],
						'label_block' => true,
					],
					
				],
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_iframe',
			[
				'label' => esc_html__( 'Map Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'iframe_link',
			[
				'label'       => __( 'Iframe Link', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Iframe Link', 'bdevs-elementor' ),
				'default'     => __( 'It is Iframe Link', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'iframe_width',
			[
				'label'       => __( 'Iframe Width', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your Iframe Width', 'bdevs-elementor' ),
				'default'     => __( 'It is Iframe Width', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'iframe_height',
			[
				'label'       => __( 'Iframe Height', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your Iframe Height', 'bdevs-elementor' ),
				'default'     => __( 'It is Iframe Height', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_image',
			[
				'label'   => esc_html__( 'Show Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_contact_form',
			[
				'label'   => esc_html__( 'Show Contact Form', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_iframe',
			[
				'label'   => esc_html__( 'Show Map', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);
		
		$this->add_control(
			'show_contact_image',
			[
				'label'   => esc_html__( 'Show Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>

		<!-- CONTACT FORM -->
		<div class="section-full  p-t120 p-b120">   
			<div class="section-content">
				<div class="container">
					<div class="contact-one">
						<!-- CONTACT FORM-->
						<div class="row no-gutters d-flex justify-content-center flex-wrap align-items-center">

							<div class="col-lg-7 col-md-12">
								<?php if (( '' !== $settings['contact_form'] ) && ( $settings['show_contact_form'] )): ?>
								<div class="contact-form-outer site-bg-gray">
									<?php echo do_shortcode(wp_kses_post($settings['contact_form'])); ?>
								</div>
								<?php endif; ?>
								<?php if (( '' !== $settings['contact_image']['url'] ) && ( $settings['show_contact_image'] )): ?>
								<div class="contact-image site-bg-gray">
									<img src="<?php echo wp_kses_post($settings['contact_image']['url']); ?>">
								</div>
								<?php endif; ?>
							</div> 

							<div class="col-lg-5 col-md-12">
								<?php if (( '' !== $settings['bg_image']['url'] ) && ( $settings['show_image'] )): ?>
								<div class="contact-info site-bg-black" style="background-image: url(<?php echo wp_kses_post($settings['bg_image']['url']); ?>);">
								<?php endif; ?>
								<?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
									<div class="section-head left wt-small-separator-outer when-bg-dark">
										<h3 class="wt-title"><?php echo wp_kses_post($settings['heading']); ?></h3>
									</div>
									<?php endif; ?>

									<div class="contact-info-section">  

										<?php foreach ( $settings['tabs'] as $item ) : ?>
										<div class="c-info-column">
											<?php if ( '' !== $item['tab_title'] ) : ?>
											<span class="m-t0"><?php echo wp_kses_post($item['tab_title']); ?></span>
											<?php endif; ?>
											<?php if ( '' !== $item['tab_subtitle'] ) : ?>
												<?php if ( '' !== $item['tab_tel_link'] && '' == $item['tab_mail_link'] ) : ?>
													<a class="c-tel-link" href="tel:<?php echo wp_kses_post($item['tab_tel_link']); ?>">
														<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
													</a>
												<?php elseif ( '' !== $item['tab_mail_link'] && '' == $item['tab_tel_link']) : ?>
													<a class="c-mail-link" href="mailto:<?php echo wp_kses_post($item['tab_mail_link']); ?>">
														<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
													</a>
												<?php else: ?>
													<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
												<?php endif; ?>
											<?php endif; ?>
										</div>  
										<?php endforeach; ?>

									</div>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>

		<!-- GOOGLE MAP -->
		<div class="section-full">
			<div class="container">   
				<div class="gmap-outline p-b120">
					<?php if (( '' !== $settings['iframe_link'] ) && ( $settings['show_iframe'] )): ?>
					<iframe src="<?php echo wp_kses_post($settings['iframe_link']); ?>" width="<?php echo wp_kses_post($settings['iframe_width']); ?>" height="<?php echo wp_kses_post($settings['iframe_height']); ?>" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
					<?php endif; ?>
				</div>
			</div>
		</div>      

	<?php
	}
}