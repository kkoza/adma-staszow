<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class Home1AboutCustom extends \BdevsElementor\Widget\Home1About {

	/**
	 * Get widget name.
	 *
	 * Retrieve Bdevs Elementor widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'home-1-about';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Bdevs Elementor widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Home 1 - About Widget', 'bdevs-elementor' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Bdevs Slider widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-favorite';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Bdevs Slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'home-1' ];
	}

	public function get_keywords() {
		return [ 'about' ];
	}

	public function get_script_depends() {
		return [ 'bdevs-elementor'];
	}

	// BDT Position
	protected function element_pack_position() {
	    $position_options = [
	        ''              => esc_html__('Default', 'bdevs-elementor'),
	        'top-left'      => esc_html__('Top Left', 'bdevs-elementor') ,
	        'top-center'    => esc_html__('Top Center', 'bdevs-elementor') ,
	        'top-right'     => esc_html__('Top Right', 'bdevs-elementor') ,
	        'center'        => esc_html__('Center', 'bdevs-elementor') ,
	        'center-left'   => esc_html__('Center Left', 'bdevs-elementor') ,
	        'center-right'  => esc_html__('Center Right', 'bdevs-elementor') ,
	        'bottom-left'   => esc_html__('Bottom Left', 'bdevs-elementor') ,
	        'bottom-center' => esc_html__('Bottom Center', 'bdevs-elementor') ,
	        'bottom-right'  => esc_html__('Bottom Right', 'bdevs-elementor') ,
	    ];

	    return $position_options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_about',
			[
				'label' => esc_html__( 'About Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'image',
			[
				'label'   => esc_html__( 'About Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::MEDIA,
				'dynamic' => [ 'active' => true ],
				'description' => esc_html__( 'Add Your About Image', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'caption_image',
			[
				'label'       => __( 'Caption Image', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your caption image', 'bdevs-elementor' ),
				'default'     => __( 'It is Caption Image', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

        $this->add_control(
			'corner_image',
			[
				'label'       => esc_html__( 'Corner Image', 'bdevs-elementor' ),
				'type'        => Controls_Manager::MEDIA,
				'dynamic' => [ 'active' => true ],
				'description' => esc_html__( 'Add Your Corner Image', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'sub_heading',
			[
				'label'       => __( 'Sub Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your sub heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Sub Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);	

		$this->add_control(
			'content',
			[
				'label'       => __( 'Content', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Content', 'bdevs-elementor' ),
				'default'     => __( 'It is Content', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);	

		$this->add_control(
			'link',
			[
				'label'       => __( 'Link', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Link', 'bdevs-elementor' ),
				'default'     => __( 'It is Link', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'button',
			[
				'label'       => __( 'Button', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Button', 'bdevs-elementor' ),
				'default'     => __( 'It is Button', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => esc_html__( 'About Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [	
					[
						'name'        => 'tab_title',
						'label'       => esc_html__( 'Tab Title', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_subtitle',
						'label'       => esc_html__( 'Tab Subtitle', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_icon',
						'label'       => esc_html__( 'Tab Icon', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
                    [
						'name'        => 'tab_logo',
						'label'       => esc_html__( 'Tab Logo', 'bdevs-elementor' ),
						'type'        => Controls_Manager::MEDIA,
						'dynamic'     => [ 'active' => true ],
						'label_block' => true
					],
				],
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_sub_heading',
			[
				'label'   => esc_html__( 'Show Sub Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_image',
			[
				'label'   => esc_html__( 'Show Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_caption_image',
			[
				'label'   => esc_html__( 'Show Caption Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

        $this->add_control(
			'show_corner_image',
			[
				'label'   => esc_html__( 'Show Corner Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);


		$this->add_control(
			'show_content',
			[
				'label'   => esc_html__( 'Show Content', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);
        

		$this->add_control(
			'show_button',
			[
				'label'   => esc_html__( 'Show Button', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>

		<div class="section-full p-t120 p-b90 bg-no-repeat bg-center bg-gray-light">
			<div class="about-section-two">
				<div class="container">
					<div class="section-content">                 
						<div class="row justify-content-center d-flex">

							<div class="col-lg-5 col-md-12 m-b30">
								<div class="about-max-two">
									<?php if (( '' !== $settings['image']['url'] ) && ( $settings['show_image'] )): ?>
									<div class="about-max-two-media"><img src="<?php echo wp_kses_post($settings['image']['url']); ?>" alt="" ></div>
									<?php endif; ?>
									<div class="about-two">
                                        <?php if (( '' !== $settings['caption_image'] ) && ( $settings['show_caption_image'] )): ?>
										<div class="about-year">
											<div class="about-year-info">                   
												<?php echo wp_kses_post($settings['caption_image']); ?>
											</div> 
										</div>
                                        <?php endif; ?>
                                        <?php if (('' !== $settings['corner_image']['url']) && ($settings['show_corner_image'])): ?>
                                            <div class="corner-image">
                                                <img src="<?php echo wp_kses_post($settings['corner_image']['url']); ?>" alt="">
                                            </div>
                                        <?php endif; ?>
									</div>
								</div>
							</div>  
							<div class="col-lg-7 col-md-12 m-b30">
								<div class="about-section-two-right">
									<div class="section-head left wt-small-separator-outer">
										<?php if (( '' !== $settings['sub_heading'] ) && ( $settings['show_sub_heading'] )): ?>
										<div class="wt-small-separator site-text-primary">
											<div  class="sep-leaf-left"></div>
											<div><?php echo wp_kses_post($settings['sub_heading']); ?></div>
										</div>
										<?php endif; ?>
										<?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
										<h2><?php echo wp_kses_post($settings['heading']); ?></h2>
										<?php endif; ?>
									</div>
									<div class="row ab-two-icon-section"> 
										<?php foreach ( $settings['tabs'] as $item ) : ?>
										<div class="col-lg-9 col-md-12">
											<div class="wt-icon-box-wraper left bg-black m-b30">
												<?php if ( '' !== $item['tab_icon'] ) : ?>
												<span class="icon-md p-t10">
													<i class="<?php echo wp_kses_post($item['tab_icon']); ?>"></i>
												</span>
                                                <?php endif; ?>
                                                <?php if ( '' !== $item['tab_logo']['url'] ) : ?>
												<span class="icon-md icon-logo p-t10">
													<img src="<?php echo wp_kses_post($item['tab_logo']['url']); ?>">
												</span>
												<?php endif; ?>
												<div class="icon-content">
													<?php if ( '' !== $item['tab_title'] ) : ?>
													<h4 class="wt-tilte"><?php echo wp_kses_post($item['tab_title']); ?></h4>
													<?php endif; ?>
													<?php if ( '' !== $item['tab_subtitle'] ) : ?>
													<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
													<?php endif; ?>
												</div>
											</div>
										</div>
										<?php endforeach; ?>
									</div>
									<div class="ab-two-info">
										<?php if (( '' !== $settings['content'] ) && ( $settings['show_content'] )): ?>
										<p><?php echo wp_kses_post($settings['content']); ?></p>
										<?php endif; ?>
										<?php if (( '' !== $settings['button'] ) && ( $settings['show_button'] )): ?>
										<a href="<?php echo wp_kses_post($settings['link']); ?>" class="site-button sb-bdr-dark"><?php echo wp_kses_post($settings['button']); ?></a>  
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div> 
				</div>
			</div>
		</div>  

	<?php
	}
}