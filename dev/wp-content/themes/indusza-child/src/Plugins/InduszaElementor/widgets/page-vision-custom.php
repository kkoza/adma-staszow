<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class PageVisionCustom extends \BdevsElementor\Widget\PageVision{


	// BDT Position
	protected function element_pack_position() {
		$position_options = [
			''              => esc_html__('Default', 'bdevs-elementor'),
			'top-left'      => esc_html__('Top Left', 'bdevs-elementor') ,
			'top-center'    => esc_html__('Top Center', 'bdevs-elementor') ,
			'top-right'     => esc_html__('Top Right', 'bdevs-elementor') ,
			'center'        => esc_html__('Center', 'bdevs-elementor') ,
			'center-left'   => esc_html__('Center Left', 'bdevs-elementor') ,
			'center-right'  => esc_html__('Center Right', 'bdevs-elementor') ,
			'bottom-left'   => esc_html__('Bottom Left', 'bdevs-elementor') ,
			'bottom-center' => esc_html__('Bottom Center', 'bdevs-elementor') ,
			'bottom-right'  => esc_html__('Bottom Right', 'bdevs-elementor') ,
		];

		return $position_options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_vision',
			[
				'label' => esc_html__( 'Vision Area', 'bdevs-elementor' ),
			]
		);

        $this->add_control(
			'top_title',
			[
				'label'       => __( 'Top Title', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Top Title', 'bdevs-elementor' ),
				'default'     => __( 'It is Top Title', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);	

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'image',
			[
				'label'   => esc_html__( 'Vision Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::MEDIA,
				'dynamic' => [ 'active' => true ],
				'description' => esc_html__( 'Add Your Vision Image', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => esc_html__( 'Vision Items', 'bdevs-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [	
					[
						'name'        => 'vision_select',
						'label'     => esc_html__( 'Vision Select', 'bdevs-elementor' ),
						'type'      => Controls_Manager::SELECT,
						'dynamic' => [ 'active' => true ],
						'options'   => [
							'1'  => esc_html__( 'Type 1', 'bdevs-elementor' ),
							'2'  => esc_html__( 'Type 2', 'bdevs-elementor' ),
                            '3'  => esc_html__( 'Type 3', 'bdevs-elementor' ),
						],
						'default'   => '1',
					],
					[
						'name'        => 'tab_title',
						'label'       => esc_html__( 'Tab Title', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_subtitle',
						'label'       => esc_html__( 'Tab Subtitle', 'bdevs-elementor' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],
					[
						'name'        => 'tab_icon',
						'label'       => esc_html__( 'Tab Icon', 'bdevs-elementor' ),
						'type'        => Controls_Manager::MEDIA,
						'dynamic'     => [ 'active' => true ],
						'default'     => esc_html__( '' , 'bdevs-elementor' ),
						'label_block' => true
					],  
                    [
                        'name'        => 'button',
                        'label'       => esc_html__( 'Button', 'bdevs-elementor' ),
                        'type'        => Controls_Manager::TEXTAREA,
                        'placeholder' => esc_html__( 'Enter your Button', 'bdevs-elementor' ),
                        'default'     => esc_html__( 'It is Button', 'bdevs-elementor' ),
                        'label_block' => true,
                    ],     
                    [
                        'name'        => 'button-link',
                        'label'       => esc_html__( 'Button Link', 'bdevs-elementor' ),
                        'type'        => Controls_Manager::TEXTAREA,
                        'placeholder' => esc_html__( 'Enter your Link', 'bdevs-elementor' ),
                        'default'     => esc_html__( 'It is Link', 'bdevs-elementor' ),
                        'label_block' => true,
                    ], 
				],
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_top_title',
			[
				'label'   => esc_html__( 'Show Sub Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_image',
			[
				'label'   => esc_html__( 'Show Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_year',
			[
				'label'   => esc_html__( 'Show Year', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);


		$this->add_control(
			'show_content',
			[
				'label'   => esc_html__( 'Show Content', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_button',
			[
				'label'   => esc_html__( 'Show Button', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings_for_display();
		?>

		<?php if (( '' !== $settings['image']['url'] ) && ( $settings['show_image'] )): ?>
		<div class="section-full p-t120  p-b90 site-bg-white" style="background-image: url(<?php echo wp_kses_post($settings['image']['url']); ?>);">
		<?php endif; ?>

            <div class="section-full  p-t50 p-b50">
                <div class="container-fluid">
                    <!-- TITLE START-->
                    <div class="section-head center wt-small-separator-outer">
                        <?php if (( '' !== $settings['top_title'] ) && ( $settings['show_top_title'] )): ?>
                        <div class="wt-small-separator site-text-primary">
                            <div class="sep-leaf-left"></div>
                            <div><?php echo wp_kses_post($settings['top_title']); ?></div>
                        </div>
                    <?php endif; ?>
                    <?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
                    <h2 class="wt-title"><?php echo wp_kses_post($settings['heading']); ?></h2>
                <?php endif; ?>
            </div>

			<div class="container">
				<div class="row d-flex justify-content-center">
					<?php foreach ( $settings['tabs'] as $item ) : ?>
					<?php if( wp_kses_post($item['vision_select']) == '1'): ?>
					<div class="col-xl-3 col-md-6 m-b30">
						<div class="service-icon-box-two text-center bg-white">
							<?php if ( '' !== $item['tab_icon']['url'] && ( $settings['show_image'] )) : ?>
							<div class="wt-icon-box-wraper">
								<div class="icon-xl inline-icon">
									<span class="icon-cell site-text-primary"><img src="<?php echo wp_kses_post($item['tab_icon']['url']); ?>"></i></span>
								</div>
							</div> 
							<?php endif; ?>
							<?php if ( '' !== $item['tab_title'] ) : ?>
							<div class="service-icon-box-title">
								<h4 class="wt-title"><?php echo wp_kses_post($item['tab_title']); ?></h4>
							</div>
							<?php endif; ?>
							<?php if ( '' !== $item['tab_subtitle'] ) : ?>
							<div class="service-icon-box-content">
								<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
							</div>
							<?php endif; ?>
                            <?php if (( '' !== $item['button'] ) && ( $settings['show_button'] )): ?>
                                <div class="p-t30"> 
                                    <a href="<?php echo wp_kses_post($item['button-link']);?>"  class="site-button sb-bdr-dark"><?php echo wp_kses_post($item['button']); ?></a>
                                </div>  
                            <?php endif; ?>
						</div>
					</div>
					<?php if( wp_kses_post($item['vision_select']) == '2'): ?>
						<div class="col-xl-3 col-md-6 m-b30">
							<div class="service-icon-box-two text-center site-bg-black">
								<?php if ( '' !== $item['tab_icon']['url'] && ( $settings['show_image'] )) : ?>
									<div class="wt-icon-box-wraper">
										<div class="icon-xl inline-icon">
											<span class="icon-cell site-text-primary"><img src="<?php echo wp_kses_post($item['tab_icon']['url']); ?>"></i></span>
										</div>
									</div> 
								<?php endif; ?>
								<?php if ( '' !== $item['tab_title'] ) : ?>
									<div class="service-icon-box-title">
										<h4 class="wt-title"><?php echo wp_kses_post($item['tab_title']); ?></h4>
									</div>
								<?php endif; ?>
								<?php if ( '' !== $item['tab_subtitle'] ) : ?>
									<div class="service-icon-box-content">
										<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
									</div>
								<?php endif; ?>
                                <?php if (( '' !== $item['button'] ) && ( $settings['show_button'] )): ?>
                                    <div class="p-t30"> 
								        <a href="<?php echo wp_kses_post($item['button-link']); ?>" class="site-button sb-bdr-dark"><?php echo wp_kses_post($item['button']); ?></a>
                                    </div> 
                                <?php endif; ?>
							</div>
						</div>
					<?php endif; ?>
                    <?php elseif( wp_kses_post($item['vision_select']) == '3'): ?>
						<div class="col-xl-3 col-lg-6 m-b30">
							<div class="service-icon-box-two text-center site-bg-transp">
								<?php if ( '' !== $item['tab_icon']['url'] && ( $settings['show_image'] )) : ?>
									<div class="wt-icon-box-wraper">
										<div class="icon-xl inline-icon">
											<span class="icon-cell site-text-primary"><img src="<?php echo wp_kses_post($item['tab_icon']['url']); ?>"></i></span>
										</div>
									</div> 
								<?php endif; ?>
								<?php if ( '' !== $item['tab_title'] ) : ?>
									<div class="service-icon-box-title">
										<h4 class="wt-title"><?php echo wp_kses_post($item['tab_title']); ?></h4>
									</div>
								<?php endif; ?>
								<?php if ( '' !== $item['tab_subtitle'] ) : ?>
									<div class="service-icon-box-content">
										<p><?php echo wp_kses_post($item['tab_subtitle']); ?></p>
									</div>
								<?php endif; ?>
                                <?php if (( '' !== $item['button'] ) && ( $settings['show_button'] )): ?>
                                    <div class="p-t30"> 
								        <a href="<?php echo wp_kses_post($item['button-link']); ?>" class="site-button sb-bdr-dark"><?php echo wp_kses_post($item['button']); ?></a>
                                    </div>
                                <?php endif; ?>
							</div>
						</div>
					<?php endif; ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>   

		
		<?php
	}
}