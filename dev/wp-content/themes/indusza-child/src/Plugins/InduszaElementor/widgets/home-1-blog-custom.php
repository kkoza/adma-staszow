<?php
namespace BdevsElementor\Widget;

use Elementor\Controls_Manager;

/**
 * Bdevs Elementor Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class Home1BlogCustom  extends \BdevsElementor\Widget\Home1Blog { 

	protected function _register_controls() {

		$this->start_controls_section(
			'section_blog',
			[
				'label' => esc_html__( 'Blog Area', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'image',
			[
				'label'   => esc_html__( 'Blog Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::MEDIA,
				'dynamic' => [ 'active' => true ],
				'description' => esc_html__( 'Add Your Blog Image', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'top_title',
			[
				'label'       => __( 'Top Title', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your Top Title', 'bdevs-elementor' ),
				'default'     => __( 'It is Top Title', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);	

		$this->add_control(
			'heading',
			[
				'label'       => __( 'Heading', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your heading', 'bdevs-elementor' ),
				'default'     => __( 'It is Heading', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);
		//custom
		$this->add_control(
			'image1_project',
			[
				'label'   => esc_html__( 'Project Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::MEDIA,
				'dynamic' => [ 'active' => true ],
				'description' => esc_html__( 'Add Your Project Image', 'bdevs-elementor' ),
			]
		);

		$this->add_control(
			'image1_title',
			[
				'label'       => __( 'Image Title', 'bdevs-elementor' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your image title', 'bdevs-elementor' ),
				'default'     => __( 'It is image title', 'bdevs-elementor' ),
				'label_block' => true,
			]
		);

		$this->end_controls_section();



		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'bdevs-elementor' ),
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'   => esc_html__( 'Alignment', 'bdevs-elementor' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => esc_html__( 'Justified', 'bdevs-elementor' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description'  => 'Use align to match position',
				'default'      => 'center',
			]
		);

		$this->add_control(
			'show_image',
			[
				'label'   => esc_html__( 'Show Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_heading',
			[
				'label'   => esc_html__( 'Show Heading', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->add_control(
			'show_top_title',
			[
				'label'   => esc_html__( 'Show Top Title', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	
		//custom

		$this->add_control(
			'show_image1_project',
			[
				'label'   => esc_html__( 'Show Project Image', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);
		$this->add_control(
			'show_image1_title',
			[
				'label'   => esc_html__( 'Show Image Title', 'bdevs-elementor' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);	

		$this->end_controls_section();

	}
	public function render() {
		$settings  = $this->get_settings_for_display();
		?>
		<div class="section-full   p-b90 bg-white">
			<?php if (( '' !== $settings['image']['url'] ) && ( $settings['show_image'] )): ?>
			<div class="half-section-top2 p-t120 site-bg-black bg-cover" style="background-image:url(<?php echo wp_kses_post($settings['image']['url']); ?>);">
			<?php endif; ?>
				<div class="container">
					<div class="section-head center wt-small-separator-outer when-bg-dark">
						<?php if (( '' !== $settings['top_title'] ) && ( $settings['show_top_title'] )): ?>
						<div class="wt-small-separator site-text-primary">
							<div class="sep-leaf-left"></div>
							<div><?php echo wp_kses_post($settings['top_title']); ?></div>
						</div>
						<?php endif; ?>
						<?php if (( '' !== $settings['heading'] ) && ( $settings['show_heading'] )): ?>
						<h2 class="wt-title"><?php echo wp_kses_post($settings['heading']); ?></h2>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="half-section-bottom2">
				<div class="container">
					<div class="section-content">
						<div class="row d-flex justify-content-center">
							<?php
							// $order = $settings['post_order'];
							// $post_number = $settings['post_number'];
							// $wp_query = new \WP_Query(array('posts_per_page' => $post_number,'post_type' => 'post',  'orderby' => 'ID', 'order' => $order));
							?>
							
								
							<!--Block one--> 
							<div class="project-image-container">
								<?php if (( '' !== $settings['image1_project']['url'] ) && ( $settings['show_image1_project'] )): ?>
									<div class="project-image-container--image"><img src="<?php echo wp_kses_post($settings['image1_project']['url']); ?>" alt=""></div>
								<?php endif; ?>
								<div class="project-image-title">
									<?php if (( '' !== $settings['image1_title'] ) && ( $settings['show_image1_title'] )): ?>
										<span class="project-image-title--title"><?php echo wp_kses_post($settings['image1_title']); ?></span>
									<?php endif; ?>
								</div>
							</div>
							
							<!-- To DO  -->
							
						</div>
					</div>
				</div>
			</div>   

	<?php
	}
}