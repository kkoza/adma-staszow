<?php

namespace InduszaChild\Plugins\InduszaElementor;

class PageContact{
    public function init()
    {
        add_action( 'elementor/widgets/widgets_registered', [ $this, 'add_custom_widget' ]);
    }

    public function add_custom_widget()
    {
        require_once get_stylesheet_directory() . '/src/Plugins/InduszaElementor/widgets/page-contact-custom.php';
        

        \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \BdevsElementor\Widget\PageContactCustom() );
    }
}
