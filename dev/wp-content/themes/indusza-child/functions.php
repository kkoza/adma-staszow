<?php

use InduszaChild\Plugins\InduszaElementor\Home1Progress;
use InduszaChild\Plugins\InduszaElementor\Home2Project;
use InduszaChild\Plugins\InduszaElementor\Home1Blog;
use InduszaChild\Plugins\InduszaElementor\PageAbout;
use InduszaChild\Plugins\InduszaElementor\PageVision;
use InduszaChild\Plugins\InduszaElementor\Home1About;
use InduszaChild\Plugins\InduszaElementor\PageContact;
use InduszaChild\Plugins\InduszaElementor\Home1Client;
use InduszaChild\Plugins\InduszaElementor\PageProject;

/**
 * Setup Indusza Child Theme's textdomain.
 *
 * Declare textdomain for this child theme.
 * Translations can be filed in the /languages/ directory.
 */
function indusza_child_theme_setup() {
	load_child_theme_textdomain( 'indusza-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'indusza_child_theme_setup' );


function indusza_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
    );
    wp_enqueue_style('Roboto Condensed', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:700');
    wp_enqueue_style('Roboto Condensed', 'https://fonts.googleapis.com/css?family=Roboto:100');
}

add_action( 'wp_enqueue_scripts', 'indusza_enqueue_styles' );

function indusza_enqueue_scripts() {

    wp_enqueue_script(
        'child-script',
        get_stylesheet_directory_uri(). '/assets/js/theme.js',
        [],
        false,
        true
    );
}

add_action( 'wp_enqueue_scripts', 'indusza_enqueue_scripts' );

require_once __DIR__ . '/src/Plugins/InduszaElementor/Home1Progress.php';
require_once __DIR__ . '/src/Plugins/InduszaElementor/Home2Project.php';
require_once __DIR__ . '/src/Plugins/InduszaElementor/Home1Blog.php';
require_once __DIR__ . '/src/Plugins/InduszaElementor/PageAbout.php';
require_once __DIR__ . '/src/Plugins/InduszaElementor/PageVision.php';
require_once __DIR__ . '/src/Plugins/InduszaElementor/Home1About.php';
require_once __DIR__ . '/src/Plugins/InduszaElementor/PageContact.php';
require_once __DIR__ . '/src/Plugins/InduszaElementor/Home1Client.php';
require_once __DIR__ . '/src/Plugins/InduszaElementor/PageProject.php';

(new Home1Blog())->init();
(new Home1Progress())->init();
(new Home2Project())->init();
(new PageAbout())->init();
(new PageVision())->init();
(new Home1About())->init();
(new PageContact())->init();
(new Home1Client())->init();
(new PageProject())->init();

