/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/components/masonry-filter.js":
/*!***************************************************!*\
  !*** ./resources/js/components/masonry-filter.js ***!
  \***************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ MasonryFilter; }
/* harmony export */ });
/**
 * If are you using link e.g https://example.com/examples/#custom-filter, script will be toggle automaticly related tab.
 */
class MasonryFilter {
  init = () => {
    window.addEventListener('load', this.run);
    window.addEventListener('hashchange', this.run);
  };
  run = () => {
    let hash = this.getHash();
    const link = this.getLink(hash);

    if (!link) {
      return;
    }

    const parent = link.parentElement;
    this.toggleTab(parent);
  };
  /**
   * If the navElement is an instance of HTMLElement, then wait one second and click it.
   * @param navElement - The element that you want to click on.
   * @returns false.
   */

  toggleTab = navElement => {
    if (!(navElement instanceof HTMLElement)) {
      return false;
    }

    setTimeout(function () {
      navElement.click();
    }, 1000);
  };
  /**
   * Get the link that has the same data-filter attribute as the hash in the URL.
   * @param hash - The hash of the current URL.
   * @returns The link that has the data-filter attribute with the value of the hash.
   */

  getLink = hash => {
    return document.querySelector(`[data-filter=".${hash}"]`);
  };
  /**
   * It gets the hash from the URL and returns it.
   * @returns The hash value of the current URL.
   */

  getHash = () => {
    let hash = window.location.hash;
    hash = hash.replace('#', '');
    return hash;
  };
}

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other entry modules.
!function() {
var __webpack_exports__ = {};
/*!*******************************!*\
  !*** ./resources/js/theme.js ***!
  \*******************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_masonry_filter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/masonry-filter */ "./resources/js/components/masonry-filter.js");

const masonryFilter = new _components_masonry_filter__WEBPACK_IMPORTED_MODULE_0__["default"]();
masonryFilter.init();
}();
// This entry need to be wrapped in an IIFE because it need to be isolated against other entry modules.
!function() {
/*!***********************************!*\
  !*** ./resources/scss/theme.scss ***!
  \***********************************/
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin

}();
/******/ })()
;
//# sourceMappingURL=theme.js.map